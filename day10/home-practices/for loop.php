<?php

/* syntax of for loop
 * for (intit; condition ;increment or decrement) {
 *  code 
 * }
 */

for($i = 10; $i <= 20; $i++){
    echo $i;
    echo '<br/>';
}
