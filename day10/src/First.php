<?php

class First {
    public $id = 10;
    private $title = 'Google Pagol Chagol';
    
    public function __construct() {     // automatic first call hobe...
        echo "Output From constructor";
    }
    
    public function hello( ) {
       echo $this->title;
    }
    
    protected function gello() {
        echo "<br/> This is gello function of First class <br/>";
            
    }
}


class mysubclass extends First{
 
    public function test() {
        $obj1 = new First();
        
        $obj1->gello();
    }
}
