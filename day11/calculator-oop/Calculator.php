<?php

class Calculator {
    public $firstNumber = "";
    public $secondNumber = "";
    
    public function assign($a,$b){
        $this->firstNumber = $a;
        $this->secondNumber = $b;
        
    }
    
    public function add(){
        return $this->firstNumber+$this->secondNumber;
    }
    
    public function sub(){
        return $this->firstNumber-$this->secondNumber;
    }
    
    public function mul(){
        return $this->firstNumber*$this->secondNumber;
    }
    
    public function div(){
        return $this->firstNumber/$this->secondNumber;
    }
    
    public function debug($data){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }                  
}
