<?php
include_once 'Calculator.php';

$obj = new Calculator();

$first_num = $_POST['first_num'];
$second_num = $_POST['second_num'];
$obj->assign($first_num, $second_num);

$obj->debug($_POST);

if(!empty($first_num) && !empty($second_num)){
    
    echo 'Addition is: ';
    echo $add = $obj->add();
    echo '<br/>';

    echo 'Subtraction is: ';
    echo $sub = $obj->sub();
    echo '<br/>';

    echo 'Multiplication is: ';
    echo $mul = $obj->mul();
    echo '<br/>';

    echo 'Division is: ';
    echo $div = $obj->div();
    echo '<br/>';

}else{
    echo "Please enter 2 valid number ";
}

