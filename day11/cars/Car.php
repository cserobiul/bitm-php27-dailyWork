<?php

class Car {
    public $carName = "";
    public $carModel = "";
    public $carManufacture = "";
    public $carColor = "";
    public $carPrice = "";
    public $carBrand = "";
    public $carWeight = "";
    public $carchachis = "";
    public $car1 = "";
    public $car2 = "";
    
    public function assign($carName,$carModel,$carManufacture,$carColor,$carPrice){
        $this->carName = $carName;
        $this->carModel = $carModel;
        $this->carManufacture = $carManufacture;
        $this->carColor = $carColor;
        $this->carPrice = $carPrice;
    }
    
    public function info(){
        echo 'Car Name: '.$this->carName;
        echo '<br/>';
        echo 'Car Model: '.$this->carModel;
        echo '<br/>';
        echo 'Car Manucacture: '.$this->carManufacture;
        echo '<br/>';
        echo 'Car Color: '.$this->carColor;
        echo '<br/>';
        echo 'Car Price: '.$this->carPrice;
    }
    
    public function debug($data){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
    
    
}
