<?php

class Car {
    public $carName = "";
    public $carModel = "";
    public $carManufacture = "";
    public $carColor = "";
    public $carPrice = "";
    public $carBrand = "";
    public $carWeight = "";
    public $carChasis= "";
    public $carFuel = "";
    public $carEngine = "";
    
    public function assign($carName,$carModel,$carManufacture,$carColor,$carPrice,$carBrand,$carWeight,$carChasis,$carFuel,$carEngine){
        $this->carName = $carName;
        $this->carModel = $carModel;
        $this->carManufacture = $carManufacture;
        $this->carColor = $carColor;
        $this->carPrice = $carPrice;
        $this->carBrand = $carBrand;
        $this->carWeight = $carWeight;
        $this->carChasis = $carChasis;
        $this->carFuel = $carFuel;
        $this->carEngine = $carEngine;
    }
    
    public function info(){
        echo 'Car Name: '.$this->carName;
        echo '<br/>';
        echo 'Car Model: '.$this->carModel;
        echo '<br/>';
        echo 'Car Manucacture: '.$this->carManufacture;
        echo '<br/>';
        echo 'Car Color: '.$this->carColor;
        echo '<br/>';
        echo 'Car Price: '.$this->carPrice;
    }
    
     public function detailsInfo(){
        echo 'Car Name: '.$this->carName;
        echo '<br/>';
        echo 'Car Model: '.$this->carModel;
        echo '<br/>';
        echo 'Car Manucacture: '.$this->carManufacture;
        echo '<br/>';
        echo 'Car Color: '.$this->carColor;
        echo '<br/>';
        echo 'Car Price: '.$this->carPrice;
        echo '<br/>';
        echo 'Car Brand: '.$this->carBrand;
        echo '<br/>';
        echo 'Car Weight: '.$this->carWeight;
        echo '<br/>';
        echo 'Car Chasis: '.$this->carChasis;
        echo '<br/>';
        echo 'Car Fuel: '.$this->carFuel;
        echo '<br/>';
        echo 'Car Engine: '.$this->carEngine;
    }
    
    public function debug($data){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
    
    
}
