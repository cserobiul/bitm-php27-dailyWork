<?php

class Student {

    public $name = "";
    public $fatherName = "";
    public $motherName = "";
    public $gender = "";
    public $age = "";
    public $height = "";
    public $weight = "";
    public $hobby = "";

    public function assign($name, $fatherName, $motherName, $gender, $age, $height, $weight, $hobby) {
        $this->name = $name;
        $this->fatherName = $fatherName;
        $this->motherName = $motherName;
        $this->gender = $gender;
        $this->age = $age;
        $this->height = $height;
        $this->weight = $weight;
        $this->hobby = $hobby;
    }

    public function information() {
        echo "Hello Dear, ";
        echo "My name is: <mark>" . $this->name;
        echo "</mark> and my age is <mark>" . $this->age;
        echo "</mark>. My father name is <mark>" . $this->fatherName;
        echo "</mark> and mother name is <mark>" . $this->motherName;
        echo "</mark>. I am <mark>" . $this->height . "</mark> cm long and my weight is <mark>" . $this->weight . "</mark> kg.";
        echo " I love <mark>" . $this->hobby . "</mark> so it's my hobby.";
    }

    public function debug($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

}
