<?php

class Student {
    public $name = "";
    public $ID = "";
    public $age = "";
    public $gender = "";
    public $education = "";
    public $bloodGroup = "";


    public function assign($name,$ID,$age,$gender,$education,$bloodGroup){
        $this->name = $name;
        $this->ID = $ID;
        $this->age = $age;
        $this->gender = $gender;
        $this->education = $education;
        $this->bloodGroup = $bloodGroup;
    }
    
    public function summarytInfo(){
        echo "Student Summary View: ";
        echo "Hello, Student Name: ".$this->name; 
        echo ' Student ID: '.$this->ID;
    }
    
    public function detailsInfo(){
        echo "<br/> Student Details View: ";
        echo "Hello, My name is: ".$this->name;        
        echo ' My ID: '.$this->ID;
        echo " My bloodgroup is: ".$this->bloodGroup;
    }
    
    public function debug($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

}