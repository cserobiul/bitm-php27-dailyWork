<?php
$color = array('white', 'green', 'red', 'blue', 'black');

echo "The memory of that scene for me is like a frame of film forever frozen at that 
    Moment: the ".$color['2']." carpet, the ".$color['1']." lawn, the ".$color['0']." house, 
        the leaden sky. 
    The new President and his first lady. - Richard M. Nixon and 
    the words ".$color['2'].", ".$color['1']." and ".$color['0']." will come from ".'$color';
