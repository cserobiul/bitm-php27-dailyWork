<?php

namespace App\seip130014\gender;

class Gender {

    public $id = "";
    public $title = "";
    public $gender = "";
    public $data = "";

    public function __construct() {
        session_start();
        $conn = mysql_connect('localhost', 'root', '') or die(mysql_error());
        $db = mysql_select_db('php27') or die(mysql_errno());
    }

    public function prepare($data = '') {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->title = $data['name'];
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }

        return $this;
    }

    public function store() {

        if (isset($this->title) && !empty($this->title)) {
            $_SESSION['name'] = $this->title;
            if (isset($this->gender) && !empty($this->gender)) {
                $query = "INSERT INTO `php27`.`gender` (`id`, `title`, `gender`,`uid`) VALUES (NULL, 
                    '" . $this->title . "', 
                    '" . $this->gender . "',
                    '" . uniqid() . "')";
                if (mysql_query($query)) {
                    $_SESSION['msg'] = '<font color="green">' . "Successfully Addeed a Member" . '</font>';
                    unset($_SESSION['name']);
                    header("LOCATION: index.php");
                } else {
                    $_SESSION['msg'] = "Opps! something going wrong";
                    header("LOCATION: create.php");
                }
            } else {
                $_SESSION['msg'] = '<font color="red">' . "Please Select Gender" . '</font>';
                header("LOCATION: create.php");
            }
        } else {
            $_SESSION['msg'] = '<font color="red">' . "Please Enter Your Name" . '</font>';
            header("LOCATION: create.php");
        }
    }

    // list view :: read data from mobile table 

    public function index() {
        $qry = mysql_query("SELECT * FROM `gender`");
        $num = mysql_num_rows($qry);

        while ($row = mysql_fetch_assoc($qry)) {
            $this->data[] = $row;
        }
        return $this->data;

        return $this;
    }

    public function show() {
        $qry = "SELECT * FROM `gender` WHERE `uid` = '" . $this->id . "'";
        $result = mysql_query($qry);

        $row = mysql_fetch_assoc($result);

        return $row;
        return $this;
    }

    public function update() {
        $qry = "UPDATE `php27`.`gender` SET 
                `title` = '" . $this->title . "',
                `gender` = '" . $this->gender . "'
                 WHERE `gender`.`uid` =  '$this->id' ";
        mysql_query($qry);
        $_SESSION['msg'] = '<font color="green">' . "Successfully Updated..." . '</font>';
        header("location: index.php");
    }

    public function delete() {
        $delQry = "DELETE FROM `php27`.`gender` WHERE `gender`.`uid` = '" . $this->id . "'";

        if (mysql_query($delQry)) {
            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: index.php");
        } else {
            $_SESSION['msg'] = '<font color="red">' . "OOPS!! Error While Deleting..." . '</font>';
            header("location: index.php");
        }
    }

    public function debug($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

}
