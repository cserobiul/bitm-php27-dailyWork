<?php

namespace App\seip130014\mobile;

use PDO;

class Mobile {

    public $id = "";
    public $title = "";
    public $data = "";
    public $username = "root";
    public $pass = "";
    public $conn = "";

    public function __construct() {
        session_start();
//        $conn = mysql_connect('localhost', 'root', '') or die(mysql_error());
//        $db = mysql_select_db('php27') or die(mysql_errno());
        $this->conn = new PDO('mysql:host=localhost;dbname=php27', $this->username, $this->pass);
    }

    public function test() {
// echo "Testing from class and method";
    }

    public function prepare($data = '') {
        if (array_key_exists('mobileName', $data)) {
            $this->title = $data['mobileName'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
//        if (isset($this->title) && !empty($this->title)) {
//            $unique_id = uniqid();
//            $query = "INSERT INTO `php27`.`mobile` (`id`,`unique_id`, `title`) VALUES (NULL,'$unique_id', '$this->title')";
//
//            if (mysql_query($query)) {
//                $_SESSION['msg'] = '<font color="green">' . "Successfully Addeed a Mobile Models" . '</font>';
//                header("LOCATION: index.php");
//            } else {
//                $_SESSION['msg'] = '<font color="red">' . "Opps! something going wrong" . '</font>';
//                header("LOCATION: index.php");
//            }
//        } else {
//            $_SESSION['msg'] = '<font color="red">' . "Opps! Value Not Set" . '</font>';
//            header("LOCATION: create.php");
//        }
//
//        return $this;
        if (isset($this->title) && !empty($this->title)) {
            try {
                $query = "INSERT INTO mobile(unique_id,title)
                    VALUES(:u, :title)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':u' => uniqid(),
                    ':title' => $this->title
                ));
                echo '<script>alert("Success")</script>';
                //header("location:index.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            $_SESSION['msg'] = '<font color="red">' . "Opps! Value Not Set" . '</font>';
            header("LOCATION: create.php");
        }
        return $this;
    }

// list view :: read data from mobile table 

    public function index() {
//        $qry = mysql_query("SELECT * FROM `mobile`");
//        $num = mysql_num_rows($qry);
//
//        while ($row = mysql_fetch_assoc($qry)) {
//            $this->data[] = $row;
//        }
//        return $this->data;
        try {
            $qry = "SELECT * FROM `mobile`";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }

    public function show() {
//        $qry = "SELECT * FROM `mobile` WHERE `unique_id` = '" . $this->id . "'";
////echo $qry;
//        $result = mysql_query($qry);
//
//        $row = mysql_fetch_assoc($result);
//
//        return $row;

        try {
            $qry = "SELECT * FROM `mobile` WHERE `unique_id` = '" . $this->id . "'";
            $q = $this->conn->query($qry) or die(mysql_error());
            $row = $q->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $row;
        return $this;
    }

    public function update() {
//        $qry = "UPDATE `php27`.`mobile` SET `title` = '" . $this->title . "' WHERE `mobile`.`unique_id` = '$this->id' ";
//        if (mysql_query($qry)) {
//            $_SESSION['msg'] = '<font color="green">' . "Successfully Updated..." . '</font>';
//            header("location: index.php");
//        }
        try {
            $upQry = "UPDATE `php27`.`mobile` SET `title` = '" . $this->title . "' WHERE `mobile`.`unique_id` = '$this->id' ";
            $q = $this->conn->query($upQry);
            $_SESSION['msg'] = '<font color="green">' . "Successfully Updated..." . '</font>';
            header("location: index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

    public function delete() {
//        $delQry = "DELETE FROM `php27`.`mobile` WHERE `mobile`.`unique_id` = '$this->id'";
//        if (mysql_query($delQry)) {
//            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
//            header("location: index.php");
//        } else {
//            $_SESSION['msg'] = '<font color="red">' . "OOPS!! Error While Deleting..." . '</font>';
//            header("location: index.php");
//        }
        try {
            $delQry = "DELETE FROM `mobile` WHERE `unique_id` = '$this->id'";
            $q = $this->conn->query($delQry) or die(mysql_error());
            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

}
