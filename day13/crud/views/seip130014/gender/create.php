<?php
session_start();
?>
<html>
    <head>
        <title>Mini Project :: Gender</title>
    </head>
    <body>
        <form method="POST" action="store.php">
            <fieldset>
                <legend>Gender</legend>
                <a href="index.php" style="text-decoration: none;">View All Member</a>              
                <?php
                if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                    echo "<h3>" . $_SESSION['msg'] . "</h3>";
                    unset($_SESSION['msg']);
                }
                ?>                                 

                <label>Enter Your Name :</label> 
                <?php
                if (empty($_SESSION['name'])) {                    
                    $_SESSION['name'] = "";
                }
                ?>
                <input name="name" type="text" value="<?php echo $_SESSION['name'];
                unset($_SESSION['name']); ?>" autofocus  > <br/>
                <label>Select Your Gender :</label> 
                Male <input name="gender" type="radio" value="male">
                Female <input name="gender" type="radio" value="female"> <br/><br/>
                <input  type="submit" name="add" value="Add Member">            
            </fieldset>
        </form>
    </body>
</html>
