<?php

include_once '../../../vendor/autoload.php';

use App\seip130014\gender\Gender;

$obj = new Gender();
$data = $obj->prepare($_GET)->show();

if (isset($data) && !empty($data)) {
    $obj->prepare($_GET)->delete();
} else {
    $_SESSION['msg'] = '<font color="red"><h2>' . "OOPS!! 404 Page..." . '</h2></font>';
    header("location: 404.php");
}
?>

