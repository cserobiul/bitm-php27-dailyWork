<?php
include_once '../../../vendor/autoload.php';

use App\seip130014\gender\Gender;

$ob = new Gender();
$data = $ob->prepare($_GET)->show();
$_SESSION['id'] = $_GET['id'];
if (isset($data) && !empty($data)) {
    ?><html>
        <head>
            <title>Mini Project :: Gender</title>
        </head>
        <body>
            <form method="POST" action="update.php">
                <fieldset>
                    <legend>Gender</legend>
                    <label>Name :</label> 
                    <input name="name" type="text" value="<?php echo $data['title'] ?>" autofocus > <br/>
                    <label>Gender :</label> 
    <?php if ($data['gender'] == 'male') { ?>
                        Male <input name="gender" type="radio" value="male"  checked="checked">
                        Female <input name="gender" type="radio" value="female">
                    <?php } elseif ($data['gender'] == 'female') { ?>
                        Male <input name="gender" type="radio" value="male">
                        Female <input name="gender" type="radio" value="female" checked="checked"> <br/>               
                    <?php } else { ?>
                        Male <input name="gender" type="radio" value="male">
                        Female <input name="gender" type="radio" value="female"> <br/>               

    <?php } ?>
                    <br/><br/>
                    <input  type="submit" name="add" value="Update">            
                </fieldset>
            </form>
        </body>
    </html>
<?php
} else {
    $_SESSION['msg'] = '<font color="red"><h2>' . "OOPS!! 404 Page..." . '</h2></font>';
    header("location: 404.php");
}
?>