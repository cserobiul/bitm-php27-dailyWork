<script>
    function delchk() {
        return confirm("Are you sure to delete??");
    }
</script> 
<?php
include_once '../../../vendor/autoload.php';

use App\seip130014\gender\Gender;

echo "<h3>Member List View</h3>";
$ob = new Gender();
$data = $ob->index();
?>
<?php
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo "<h4>" . $_SESSION['msg'] . "</h4>";
    unset($_SESSION['msg']);
}
?>
<?php if (isset($data) && !empty($data)) { ?>
    <table border="1" cellpadding="5">
        <tr>
            <th>Name</th>
            <th>Gender</th>
            <th colspan="3">Action</th>
        </tr>   

        <?php
        foreach ($data as $item) {
            ?>
            <tr>
                <td><?php echo ucwords($item['title']); ?></td>
                <td><?php echo ucwords($item['gender']); ?></td>
                <td><a href="show.php?id=<?php echo $item['uid']; ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $item['uid']; ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $item['uid']; ?>" onclick="return delchk()">Delete</a></td>
            </tr>       
    <?php } ?> 

    </table>
<?php
} else {
    echo '<font color="red">' . "Empty database" . '</font>';
}
?>
<br/>

<a href="create.php">Add New</a>