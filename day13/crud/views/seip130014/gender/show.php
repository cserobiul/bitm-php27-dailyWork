<?php
include_once '../../../vendor/autoload.php';
use App\seip130014\gender\Gender;
$myObj = new Gender();
$data = $myObj->prepare($_GET)->show();

if(isset($data) && !empty($data)){
    echo "<h3>Member Single View</h3>";
    ?>
<table border="1" cellpadding="5" >
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Gender</th>
    </tr>
    
    <tr>
        <td><?php echo $data['id']; ?></td>
        <td><?php echo ucwords($data['title']); ?></td>
        <td><?php echo ucwords($data['gender']); ?></td>
    </tr>
</table>
    <a href="index.php" style="text-decoration: none;">View All Member</a><br/>
    
<?php }else{
    $_SESSION['msg'] = '<font color="red"><h2>' . "OOPS!! 404 Page..." . '</h2></font>';
    header("location: 404.php");
}
?>


