<?php

include_once '../../../vendor/autoload.php';

use App\seip130014\gender\Gender;

$obj = new Gender();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $obj->prepare($_POST);
    $data = $obj->store();
} else {
    $_SESSION['msg'] = '<font color="red"><h2>' . "OOPS!! 404 Page..." . '</h2></font>';
    header("location: 404.php");
}

