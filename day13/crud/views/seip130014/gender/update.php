<?php

include_once '../../../vendor/autoload.php';

use App\seip130014\gender\Gender;

$ob = new Gender();
if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
    $_POST['id'] = $_SESSION['id'];
    $data = $ob->prepare($_POST)->update();
} else {
    $_SESSION['msg'] = '<font color="red"><h2>' . "OOPS!! 404 Page..." . '</h2></font>';
    header("location: 404.php");
}

