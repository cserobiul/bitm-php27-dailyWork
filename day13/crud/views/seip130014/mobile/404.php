<?php

session_start();
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo "<h3>" . $_SESSION['msg'] . "</h3>";
    unset($_SESSION['msg']);
} else {
    header("location: index.php");
}
