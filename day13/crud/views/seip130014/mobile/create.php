<?php
session_start();
?>
<html>
    <head>
        <title>Favorite Mobile Models</title>
    </head>
    <body>
        <form method="POST" action="store2.php">
            <fieldset>
                <legend>Add Favorite Mobile Models</legend>
                <a href="index.php" style="text-decoration: none;">View All Models</a><br/>
                <?php
                if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                    echo "<h3>" . $_SESSION['msg'] . "</h3>";
                    unset($_SESSION['msg']);
                }
                ?>
                <label>Enter Mobile Model :</label> 
                <input name="mobileName" type="text" autofocus >

                <input  type="submit"  value="Add">            
            </fieldset>
        </form>
    </body>
</html>