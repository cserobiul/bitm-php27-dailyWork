<?php

include_once '../../../vendor/autoload.php';

use App\seip130014\mobile\Mobile;

$obj = new Mobile();

$data = $obj->prepare($_GET)->show();
if (isset($data) && !empty($data)) {
    if (isset($_GET['id'])) {
        $obj->prepare($_GET)->delete();
    } else {
        $_SESSION['msg'] = "Opps you are not authorized for this page...";
        header("location: 404.php");
    }
} else {
    $_SESSION['msg'] = "Opps you are not authorized for this page... ";
    header("location: 404.php");
}
?>


