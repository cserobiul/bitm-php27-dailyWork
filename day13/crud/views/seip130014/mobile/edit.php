<?php
include_once '../../../vendor/autoload.php';

use App\seip130014\mobile\Mobile;

$ob = new Mobile();

$data = $ob->prepare($_GET)->show();
$_SESSION['id'] = $_GET['id'];
if (isset($data) && !empty($data)) {
    ?>
    <html>
        <head>
            <title>Favorite Mobile Models</title>
        </head>
        <body>
            <form method="POST" action="update.php">
                <fieldset>
                    <legend>Edit Favorite Mobile Models</legend>
                    <a href="index.php" style="text-decoration: none;">View All Models</a><br/>
                    <?php
                    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                        echo "<h3>" . $_SESSION['msg'] . "</h3>";
                        unset($_SESSION['msg']);
                    }
                    ?>
                    <label>Mobile Model :</label> 
                    <input name="mobileName" type="text" value="<?php echo $data['title']; ?>" autofocus >                                
                    <input  type="submit" name="add" value="Update">            
                </fieldset>
            </form>
        </body>
    </html>
<?php
} else {
    $_SESSION['msg'] = "Opps you are not authorized for this page...";
    header("location: 404.php");
}
?>