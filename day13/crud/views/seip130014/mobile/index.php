<script>
    function delchk() {
        return confirm("Are you sure to delete??");
    }
</script> 
<?php
include_once '../../../vendor/autoload.php';

use App\seip130014\mobile\Mobile;

$ob = new Mobile();
$data = $ob->index();
?>
<?php
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo "<h4>" . $_SESSION['msg'] . "</h4>";
    unset($_SESSION['msg']);
}
?>
<?php if (isset($data) && !empty($data)) { ?>
    <h3>Mobile Models List View</h3>
    <table border="1" cellpadding="5">
        <tr>
            <th>Mobile Models</th>
            <th colspan="3">Action</th>
        </tr>   

        <?php
        foreach ($data as $item) {
            ?>
            <tr>
                <td><?php echo ucwords($item['title']); ?></td>
                <td><a href="show.php?id=<?php echo $item['unique_id']; ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $item['unique_id']; ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $item['unique_id']; ?>" onclick="return delchk()">Delete</a></td>
            </tr>       
        <?php } ?> 

    </table>
    <?php
} else {
    echo '<font color="red">' . "<h2>Empty database</h2>" . '</font>';
}
?>
<br/>
<a href="create.php" style="text-decoration: none;">Add New Mobile Model</a>

