<?php
include_once '../../../vendor/autoload.php';
use App\seip130014\mobile\Mobile;
$myObj = new Mobile();
$data = $myObj->prepare($_GET)->show();

if (isset($data) && !empty($data)) {
    echo "<h3>Mobile Models Single View</h3>";
    ?>
    <table border="1" cellpadding="5" >
        <tr>
            <th>ID</th>
            <th>Title</th>
        </tr>
        <tr>
            <td><?php echo $data['id']; ?></td>
            <td><?php echo ucwords($data['title']); ?></td>
        </tr>
    </table>
    <a href="index.php" style="text-decoration: none;">View All Models</a><br/>
    <?php
} else {
    $_SESSION['msg'] = "Opps you are not authorized for this page...";
    header("location: 404.php");
}
?>