<?php
include_once '../../../vendor/autoload.php';
use App\seip130014\mobile\Mobile;
$obj = new Mobile();

if($_SERVER['REQUEST_METHOD'] == "POST"){    
    $obj->prepare($_POST);
    $obj->store();
}else{
    $_SESSION['msg'] = "Opps you are not authorized for this page...";
    header("location: 404.php");
}


