<?php

include_once '../../../vendor/autoload.php';

use App\seip130014\mobile\Mobile;

$ob = new Mobile();
if (isset($_SESSION['id'])) {
    $_POST['id'] = $_SESSION['id'];
    unset($_SESSION['id']);
    $ob->prepare($_POST)->update();
} else {
    $_SESSION['msg'] = "Opps you are not authorized for this page...";
    header("location: 404.php");
}