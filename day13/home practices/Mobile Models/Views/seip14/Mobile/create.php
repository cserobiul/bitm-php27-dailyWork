<?php
    session_start();
?>
<html>
    <head>
        <title>Mobile</title>
    </head>
    <body>
        <fieldset>
            <legend>Add Mobile Model and Price</legend>
            <a href="index.php" >View All Mobile Models List</a> <br/><br/>
            <form method="POST" action="store.php">
                <?php
                    if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
                        echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                        echo '<a href="create.php" >Dismis Message</a> <br/>';
                    }
                ?>
                <label>Mobile Name</label>
                <input name="mobileModel" type="text" autofocus>
                <label>Mobile Price </label>
                <input name="mobilePrice" type="number" autofocus >
                <input name="add" type="submit"value="Add" >
            </form>
        </fieldset>
    </body>
</html>
