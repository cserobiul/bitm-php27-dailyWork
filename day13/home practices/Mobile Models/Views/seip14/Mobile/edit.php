<?php
    include_once '../../../Src/seip14/Mobile/Mobile.php';
    
    $myObj = new Mobile();
    
    $data = $myObj->assign($_GET)->show();
?>
<html>
    <head>
        <title>Mobile</title>
    </head>
    <body>
        <fieldset>
            <legend>Edit Mobile Model and Price</legend>
            <form method="POST" action="update.php">
                <label>Mobile Name</label>
                <input name="mobileModel" type="text" value="<?php echo $data['title'] ?>" autofocus>
                <label>Mobile Price </label>
                <input name="mobilePrice" type="number" value="<?php echo $data['price'] ?>" autofocus >
                <input name="id" type="hidden" value="<?php echo $data['id'] ?>" >
                <input name="update" type="submit"value="Update" >
            </form>
        </fieldset>
    </body>
</html>
