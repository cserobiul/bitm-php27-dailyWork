<h2> All Mobile Models List </h2>
<script>
    function delchk() {
        return confirm("Are you sure to delete??");
    }
</script>    
<?php
include_once '../../../Src/seip14/Mobile/Mobile.php';

$ob = new Mobile();

$data = $ob->index();

if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
    echo '<a href="index.php" >Dismis Message</a> <br/>';
}
?>
<?php
if (isset($data) && !empty($data)) {
    ?>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Mobile Model</th>
            <th>Price</th>
            <th colspan="3">Action</th>
        </tr>
        <?php foreach ($data as $value) { ?>
            <tr>
                <td><?php echo $value['id']; ?></td>
                <td><?php echo ucwords($value['title']); ?></td>
                <td><?php echo $value['price'] . ' TK'; ?></td>

                <td><a href="show.php?id=<?php echo $value['id']; ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $value['id']; ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $value['id']; ?>" onclick="return delchk()">Delete</a></td>
            </tr>
    <?php } ?>
    </table>
        <?php
    } else {
        echo '<font color="red">' . "Empty Database..." . '</font>';
    }
    ?>
<a href="create.php"><br/> Add New Mobile Models </a>