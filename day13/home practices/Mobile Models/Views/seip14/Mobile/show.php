<h3>Mobile Models Single View</h3>
<?php
include_once '../../../Src/seip14/Mobile/Mobile.php';

$myObj = new Mobile();

$data = $myObj->assign($_GET)->show();
//$myObj->debug($data);
?>
<?php if(isset($data) && !empty($data)){ ?>
<table border="1" cellpadding="5" >
    <tr>
        <th>ID</th>
        <th>Mobile Model</th>
        <th>Price</th>
    </tr>
    
    <tr>
        <td><?php echo $data['id']; ?></td>
        <td><?php echo ucwords($data['title']); ?></td>        
        <td><?php echo ucwords($data['price']); ?></td>

    </tr>
</table>
<?php }else{
    echo '<font color="red" size="20">'."OOPS 404...".'</font>';
}
    ?></br>
<a href="index.php">View All Models</a><br/>
