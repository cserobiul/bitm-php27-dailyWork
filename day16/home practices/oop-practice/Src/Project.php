<?php
namespace App;
class Project {
    public $title = "";
    public $status = "";
    
    public function __construct($title = "Default Project"){
        $this->title = $title;
        $this->status = "Pending";
    }
}
