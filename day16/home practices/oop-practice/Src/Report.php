<?php
namespace App;
class Report {
    public $title = "";
    public $status = "";
    
    public function __construct($title = "Default Report") {
        $this->title = $title;
        $this->status = "Active";
    }
}
