<?php

/* Technique one
include_once ('Src/Project.php');
include_once ('Src/Report.php');
*/

/* Technique two 
function __autoload($myClass){
    list($namespace,$className) = explode("\\", $myClass);
    include_once ("Src/".$className.".php");
}*/

// technique three  :: we use it everytime
include_once './vendor/autoload.php';

use App\Project;
$obj = new Project();
echo 'Project Name = '.$obj->title;

echo "<br/>";

use App\Report;
$obj2 = new Report();
echo 'Report Name = '.$obj2->title;
