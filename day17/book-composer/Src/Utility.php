<?php
namespace App;
class Utility {
    public function __construct() {
        echo "Utility Class";
    }

    public function debug($data = ''){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}
