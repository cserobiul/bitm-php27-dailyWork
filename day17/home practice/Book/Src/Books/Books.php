<?php

namespace App\Books;

class Books {

    public $id = "";
    public $name = "";
    public $author = "";
    public $price = "";
    public $data = "";

    public function __construct() {
        session_start();
        $conn = mysql_connect('localhost', 'root', '') or die(mysql_error());
        $db = mysql_select_db('php27') or die(mysql_error());
    }

    public function prepare($data = '') {
        if (array_key_exists('bookName', $data)) {
            $this->name = $data['bookName'];
        }
        if (array_key_exists('bookAuthor', $data)) {
            $this->author = $data['bookAuthor'];
        }
        if (array_key_exists('bookPrice', $data)) {
            $this->price = $data['bookPrice'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
        if (isset($this->name) && !empty($this->name) &&
                isset($this->author) && !empty($this->author) &&
                isset($this->price) && !empty($this->price)
        ) {
            $qry = "INSERT INTO `book` (`name`,`author`,`price`) VALUES('$this->name','$this->author','$this->price' ) ";
            if (mysql_query($qry)) {
                $_SESSION['msg'] = '<font color="green">' . "Successfully Book Added..." . '</font>';
                header("location: index.php");
            } else {
                $_SESSION['msg'] = '<font color="red">' . "Error while Book Added..." . '</font>';
                header("location: create.php");
            }
        } else {
            $_SESSION['msg'] = '<font color="red">' . "Value not set..." . '</font>';
            header("location: create.php");
        }
        return $this;
    }

    public function index() {
        $qry = "SELECT * FROM `book` ";
        $result = mysql_query($qry);
        while ($row = mysql_fetch_assoc($result)) {
            $this->data[] = $row;
        }return $this->data;
        return $this;
    }

    public function show() {
        $qry = "SELECT * FROM `book` WHERE id = '$this->id' ";
        $result = mysql_query($qry);
        $row = mysql_fetch_assoc($result);
        return $row;
        return $this;
    }

    public function delete() {
        $qry = "DELETE FROM `book` WHERE id = '$this->id' ";
        if (mysql_query($qry)) {
            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: index.php");
        } else {
            $_SESSION['msg'] = '<font color="red">' . "Error while Deleted..." . '</font>';
            header("location: index.php");
        }return $this;
    }

    public function update() {
        $qry = "UPDATE `book` SET 
                `name`='$this->name',
                `author`='$this->author',
                `price`='$this->price'
                WHERE `id` = '$this->id' 
                ";
        if (mysql_query($qry)) {
            $_SESSION['msg'] = '<font color="green">' . "Successfully Updated..." . '</font>';
            header("location: index.php");
        } else {
            $_SESSION['msg'] = '<font color="red">' . "Error while Updated..." . '</font>';
            header("location: index.php");
        }
    }

}
