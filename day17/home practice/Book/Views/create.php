<html>
    <head>
        <title>Book</title>
    </head>
    <body>
        <fieldset>
            <legend>Add Book</legend>
            <?php
            session_start();
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
            ?>
            <form action="store.php" method="post">
                <label>Book Name</label>
                <input name="bookName"   type="text" autofocus>

                <label>Book Author</label>
                <input name="bookAuthor"   type="text" autofocus>

                <label>Book Price</label>
                <input name="bookPrice"   type="text" autofocus>
                <input type="submit" value="Add New Book">

            </form>
            <a href="index.php">View All Books</a>
        </fieldset>
    </body>
</html>
