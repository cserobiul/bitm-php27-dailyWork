<?php
include_once ('../vendor/autoload.php');
$obj8 = new App\Books\Books();
$_SESSION['id'] = $_GET['id'];
$data = $obj8->prepare($_GET)->show();
?>
<fieldset>
    <legend>Add Book</legend>
    <form action="update.php" method="post">
        <label>Book Name</label>
        <input name="bookName"   type="text" value="<?php echo $data['name']; ?>" autofocus>

        <label>Book Author</label>
        <input name="bookAuthor"   type="text" value="<?php echo $data['author']; ?>" autofocus>

        <label>Book Price</label>
        <input name="bookPrice"   type="text" value="<?php echo $data['price']; ?>" autofocus>
        <input type="submit" value="Update Book">

    </form>
</fieldset>
