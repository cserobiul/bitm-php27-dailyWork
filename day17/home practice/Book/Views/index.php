<h2>View All Books List</h2>
<?php
include_once '../vendor/autoload.php';
$obj5 = new App\Books\Books();
$data = $obj5->index();
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}

if (isset($data) && !empty($data)) {
    ?>
    <table border="1"   cellpadding="5">
        <tr>
            <th>Book Name</th>
            <th>Book Author</th>
            <th>Book Price</th>
            <th colspan="3">Action</th>
        </tr>

        <?php foreach ($data as $item) { ?>
            <tr>
                <td><?php echo ucwords($item['name']); ?></td>
                <td><?php echo ucwords($item['author']); ?></td>
                <td><?php echo $item['price']; ?> TK</td>
                <td><a href="show.php?id=<?php echo $item['id']; ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $item['id']; ?>">Eidt</a></td>
                <td><a href="delete.php?id=<?php echo $item['id']; ?>">Delete</a></td>

            </tr>
        <?php }
        ?>

    </table>

    <?php
} else {
    $_SESSION['msg'] = '<font color="red">' . "Database..." . '</font>';
}
?>



<a href="create.php">Add New Book</a>

