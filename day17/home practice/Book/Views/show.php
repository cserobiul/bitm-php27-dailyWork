<?php
include_once ('../vendor/autoload.php');
$obj6 = new App\Books\Books();
$data = $obj6->prepare($_GET)->show();
?>
<table border="1" cellpadding="5">
    <tr>
        <th>Book Name</th>
        <th>Book Author</th>
        <th>Book Price</th>
    </tr>  
    <tr>
        <td><?php echo ucwords($data['name']); ?></td>
        <td><?php echo ucwords($data['author']); ?></td>
        <td><?php echo $data['price']; ?> TK</td>
    </tr>
        
</table>
<a href="index.php">View All Books</a>
