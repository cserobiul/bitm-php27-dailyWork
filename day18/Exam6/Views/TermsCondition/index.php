<h2>All Name List</h2>
<?php
include_once ('../../Src/TermsCondition/Terms.php');
$obj1 = new App\TermsCondition\Terms();
$data = $obj1->index();
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>
<table border="1" cellpadding="5">
    <tr>
        <th>Name</th>
        <th>Terms & Condition</th>
        <th colspan="3">Action</th>
    </tr>
    <?php foreach ($data as $item) {
        ?>
        <tr>
            <td><?php echo ucwords($item['name']); ?></td>

            <th>
            <?php
                if($item['tc'] == 'checked' ){ ?>
         <input type="checkbox" name="tc" value="checked" checked>
             <?php }else{ ?>
         <input type="checkbox" name="tc" value="checked">
             <?php }?>
            </th>

            <td><a href="show.php?id=<?php echo $item['id']; ?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $item['id']; ?>">Edit</a></td>
            <td><a href="delete.php?id=<?php echo $item['id']; ?>">Delete</a></td>
        </tr>
    <?php } ?> 
</table>

<a href="create.php">Add New</a>
