﻿<?php

    echo '<b>empty function:</b> ';
    
    $var = '';
    if(empty($var)){
        echo '$var is either 0, emppty, or not set at all';        
    }else{
        echo '$var is not empty';
    }
   
    echo '<br/>';
   echo '<b>gettype function:</b> ';
    echo gettype($var);

     echo '<br/>';
    echo 'is_array function:';
    echo '<br/>';
    
    $data = array('apple','banana','orange');
    echo is_array($data) ? 'this is an array' : 'it is not an array';

    
     echo '<br/>';
     echo 'is_bool function:';
     echo '<br/>';
     
     if(is_bool($var)===true){
         echo 'yes, this is a boolean ';
     }else{
         echo 'sorry, this is not a boolena';
     }
    
      echo '<br/>';
      echo 'is_float function:';
      echo '<br/>';
      
      $mark =89.8;
      if(is_float($mark)){
          echo 'float value';
      }else{
          echo 'is not float value';
      }
      
      echo '<br/>';
      echo 'isset function:';
          echo '<br/>';
      
      if(isset($var)){
          echo "this variable set so I will print.";
      }else{
          echo 'sorry variable in not set !!';
      }
      
      echo '<br/>';
      echo 'print_r function:';
      
      echo '<pre>';
        $a = array('a' => 'apple', 'b'=>'ball','c' => array('x','y','z'));
        print_r($a);
      //echo '</pre>';
      
      echo '<br/>';
      echo 'var_dump function:';
      echo '<br/>';
      
      $variable =array(1,2,array("a","b","c","d"));
      var_dump($variable);
?>