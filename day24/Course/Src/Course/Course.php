<?php

namespace App\Course;

use PDO;

class Course {

    public $id = "";
    public $name = "";
    public $semester = "";
    public $weaver = "";
    public $data = "";
    public $user = "root";
    public $pass = "";
    public $conn = "";

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=php27', $this->user, $this->pass);
    }

    // assgin value
    public function assign($data) {
        if (!empty($data['name'])) {
            $this->name = $data['name'];
        } else {
            $_SESSION['name'] = '<font color="red">' . "Name Required" . '</font>';
        }

        if (!empty($data['semester'])) {
            $this->semester = $data['semester'];
        } else {
            $_SESSION['semester'] = '<font color="red">' . "Semester Required" . '</font>';
        }

        if (!empty($data['weaver'])) {
            $this->weaver = $data['weaver'];
        } else {
            $this->weaver = 'No';
        }


        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        return $this;
    }

    // store value into database 
    public function store() {
        if (!empty($this->name) && !empty($this->semester)) {
            try {
                $query = "INSERT INTO course(name,semester,weaver,u_id)
                    VALUES(:name, :semester, :weaver, :u_id)";
                $q = $this->conn->prepare($query);
                $q->execute(array(
                    ':name' => $this->name,
                    ':semester' => $this->semester,
                    ':weaver' => $this->weaver,
                    ':u_id' => uniqid()
                ));
                $_SESSION['msg'] = '<font color="green">' . "Successfully Submitted" . '</font>';
                header("LOCATION: index.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("LOCATION: create.php");
        }
        return $this;
    }

    // get all data from database
    public function index() {
        try {
            $qry = "SELECT * FROM `course`  ORDER BY `course`.`name` ASC";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }

    // get single data
    public function show() {
        try {
            $qry = "SELECT * FROM `course` WHERE `u_id` = '" . $this->id . "'";
            $q = $this->conn->query($qry) or die(mysql_error());
            $row = $q->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $row;
        return $this;
    }

    public function update() {
        if (!empty($this->name) && !empty($this->semester)) {
            try {
//                $upQry = "UPDATE `course` SET 
//                    `name` = '" . $this->name . "',
//                    `semester` = '" . $this->semester . "',
//                    `weaver` = '" . $this->weaver . "'
//                        
//                    WHERE `course`.`u_id` = '$this->id' ";  
//                $q = $this->conn->query($upQry);
                $upQry = "UPDATE course SET 
                        name = :name, 
                        semester = :semester, 
                        weaver = :weaver 
                        WHERE u_id = :uid";
                $q = $this->conn->prepare($upQry);
                $q->execute(array(
                    ':name' => $this->name,
                    ':semester' => $this->semester,
                    ':weaver' => $this->weaver,
                    ':uid'  => $this->id
                )) or die('Error While Updateed');
                
                $_SESSION['msg'] = '<font color="green">' . "Successfully Updated..." . '</font>';
                header("location: index.php");
            } catch (Exception $ex) {
                echo 'Error: ' . $ex->getMessage();
            }
        } else {
            $_SESSION['msg'] = '<font color="red">' . "Unauthorize User" . "</font>";
            header("LOCATION: index.php");
        }
        return $this;
    }

    // delete form database
    public function delete() {
        try {
            $delQry = "DELETE FROM `course` WHERE u_id = :id";
            $q = $this->conn->prepare($delQry);
            $q->bindParam(':id', $this->id);
            $q->execute() or die("Error While Deleting");            
            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

}
