<?php
session_start();
?>
<form method="POST" action="store.php">
    <fieldset>
        <legend>Course Registration Form</legend>
        <label>Name: </label>
        <input type="text" name="name" autofocus>
        <?php 
            if(isset($_SESSION['name']) && !empty($_SESSION['name'])){
                echo $_SESSION['name'];
                unset($_SESSION['name']);
            }
        ?>
        <br>
        <label>Semester</label>
        <select name="semester">
            <option value="">Select Semester</option>
            <option value="semester1">Semester 1 [Weaver 10 %]</option>    
            <option value="semester2">Semester 2 [Weaver 15 %]</option>
            <option value="semester3">Semester 3 [Weaver 30 %]</option>
        </select>
        <?php 
            if(isset($_SESSION['semester']) && !empty($_SESSION['semester'])){
                echo $_SESSION['semester'];
                unset($_SESSION['semester']);
            }
        ?>
        <br>
        <input type="checkbox" name="weaver" value="Yes">Get Weaver
        <br>
        <input type="submit" value="Registration">
    </fieldset>
</form>

<a href="index.php">View All</a>
