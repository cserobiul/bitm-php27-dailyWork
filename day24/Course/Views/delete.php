<?php

include_once ('../vendor/autoload.php');
$deleteObj = new App\Course\Course();

$data = $deleteObj->assign($_GET)->show();

if (isset($data) && !empty($data)) {
    $deleteObj->assign($_GET)->delete();
} else {
    $_SESSION['msg'] = '<font color="red">' . "Unauthorize User" . "</font>";
    header("location:index.php");
}
