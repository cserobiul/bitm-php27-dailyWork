<?php
include_once ('../vendor/autoload.php');
$editObj = new App\Course\Course();

$data = $editObj->assign($_GET)->show();
$_SESSION['id'] = $_GET['id'];
if (isset($data) && !empty($data)) {
    ?>

    <form method="POST" action="update.php">
        <fieldset>
            <legend>Course Registration Form</legend>
            <label>Name: </label>
            <input type="text" name="name" value="<?php echo $data['name'] ?>" autofocus>

            <br>
            <label>Semester</label>
            <select name="semester">
                <option value="<?php echo $data['semester'] ?>"><?php echo $data['semester'] ?></option>
                <option value="semester1">Semester 1 [Weaver 10 %]</option>    
                <option value="semester2">Semester 2 [Weaver 15 %]</option>
                <option value="semester3">Semester 3 [Weaver 30 %]</option>
            </select>

            <br>
            <?php 
            if($data['weaver'] == "Yes"){ ?>
                <input type="checkbox" name="weaver" value="Yes" checked>Get Weaver
            <?php }elseif($data['weaver'] == "No"){ ?>
                <input type="checkbox" name="weaver" value="Yes">Get Weaver
            <?php }else{ ?>
                <input type="checkbox" name="weaver" value="Yes" >Get Weaver
                      
            <?php }
            ?>

            <br>
            <input type="submit" value="Update">
        </fieldset>
    </form>
    <?php
} else {
    $_SESSION['msg'] = '<h2><font color="red">' . "Unauthorize User" . "</font></h2>";
    header("location:index.php");
}
?>
<a href="index.php">View All</a>
