<?php
include_once ('../vendor/autoload.php');
$dataObj = new App\Course\Course();

$data = $dataObj->index();
//echo "<pre>";
//print_r($data);
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo '<h2>'.$_SESSION['msg'].'</h2>';
    unset($_SESSION['msg']);
}
?>

<table border="1" cellpadding="5">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Weaver</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $sl = 1;
    if (isset($data) && !empty($data)) {
        foreach ($data as $item) {
            ?>
            <tr>
                <td><?php echo $sl++; ?></td>
                <td><?php echo ucwords($item['name']); ?></td>
                <td><?php echo ucfirst($item['semester']) ?></td>
                <td>
                    <?php if($item['weaver'] == 'Yes' ){
                        echo 'Yes';
                    }else{
                        echo 'No';
                    } ?>
                </td>

                <td><a href="show.php?id=<?php echo $item['u_id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $item['u_id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $item['u_id'] ?>">Delete</a></td>


            </tr>
        <?php
        }
    } else {
        echo '<tr><td colspan="8"><font color="red">Empty Database</font></td></tr>';
    }
    ?>  


</table>


<br>
<a href="create.php">Add New</a>
