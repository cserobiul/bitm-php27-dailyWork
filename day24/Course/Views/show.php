<?php
include_once ('../vendor/autoload.php');
$showObj = new App\Course\Course();

$data = $showObj->assign($_GET)->show();
//echo "<pre>";
//print_r($data);

if (isset($data) && !empty($data)) {
    ?>
    <table border="1" cellpadding="5">
        <tr>
            <th>Name</th>
            <th>Semester</th>
            <th>Weaver</th>
            <th>Semester Cost</th>
            <th>Get Weaver</th>
            <th>Total Cost</th>
        </tr>

        <tr>
            <td><?php echo ucwords($data['name']); ?></td>
            <td><?php echo ucfirst($data['semester']); ?></td>
            <td><?php echo $data['weaver']; ?></td>
            <td>
                <?php
                if ($data['semester'] == 'semester1') {
                    $semester = '9,000 Tk';
                    echo $semester;
                } elseif ($data['semester'] == 'semester2') {
                    echo $semester = '11,000 Tk';
                } elseif ($data['semester'] == 'semester3') {
                    echo $semester = '13,000 Tk';
                } else {
                    $semester = 'NA';
                    echo 'NA';
                }
                ?>
            </td>

            <td>
                <?php
                if (($data['semester'] == 'semester1') && $data['weaver'] == 'Yes') {
                    $semester_weaver = 9000 * 10 / 100;
                    echo $semester_weaver;
                    echo ' Tk (10%)';
                } elseif (($data['semester'] == 'semester2') && $data['weaver'] == 'Yes') {
                    echo $semester_weaver = 11000 * 15 / 100;
                    echo ' Tk (15%)';
                } elseif (($data['semester'] == 'semester3') && $data['weaver'] == 'Yes') {
                    echo $semester_weaver = 13000 * 30 / 100;
                    echo ' Tk (30%)';
                } else {
                    $semester_weaver = 'NA';
                    echo 'NA';
                }
                ?>
            </td>
            <td>
                <?php
                if (($data['semester'] == 'semester1') && $data['weaver'] == 'Yes') {
                    echo '8,100 Tk';
                } elseif (($data['semester'] == 'semester2') && $data['weaver'] == 'Yes') {
                    echo '9,350 Tk';
                } elseif (($data['semester'] == 'semester3') && $data['weaver'] == 'Yes') {
                    echo '9,100 Tk';                    
                } else {
                   echo $semester;
                }
                ?>
            </td>
        </tr>


    </table>
    <?php
} else {
    $_SESSION['msg'] = '<font color="red">' . "Unauthorize User" . "</font>";
    header("location:index.php");
}
?>
<a href="index.php">View All</a>