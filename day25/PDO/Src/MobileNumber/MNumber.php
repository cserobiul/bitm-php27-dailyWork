<?php

namespace App\MobileNumber;

use PDO;

class MNumber {

    public $id = "";
    public $name = "";
    public $number = "";
    public $is_delete = "";
    public $data = "";
    public $user = "root";
    public $pass = "";

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=php27', $this->user, $this->pass);
        $Vdate = date("Y-m-d g:i:s");
    }

    // assgin value
    public function assign($data) {
        if (!empty($data['name'])) {
            $this->name = $data['name'];
        } else {
            $_SESSION['name'] = '<font color="red">' . "Name Required" . '</font>';
        }

        if (!empty($data['number'])) {
            $this->number = $data['number'];
        } else {
            $_SESSION['number'] = '<font color="red">' . "Mobile Number Required" . '</font>';
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        if (!empty($data['is_delete'])) {
            $this->is_delete = $data['is_delete'];
        } else {
            $this->is_delete = 0;
        }

        return $this;
    }

    // store value into database 
    public function store() {
        if (!empty($this->name) && !empty($this->number)) {
            try {
                $query = "INSERT INTO number(name,number,u_id,is_delete,date)
                    VALUES(:name, :number, :u_id, :is_delete,:date)";
                $q = $this->conn->prepare($query);
                $q->execute(array(
                    ':name' => $this->name,
                    ':number' => $this->number,
                    ':u_id' => uniqid(),
                    ':is_delete' => $this->is_delete,
                    ':date' => date("Y-m-d g:i:s")
                ));
                $_SESSION['msg'] = '<font color="green">' . "Successfully Submitted" . '</font>';
                header("LOCATION: index.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("LOCATION: create.php");
        }
        return $this;
    }

    // get all data from database
    public function index() {
        try {
            $qry = "SELECT * FROM `number` WHERE is_delete=0 ORDER BY `date` DESC";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }

    // get all data from database
    public function tIndex() {
        try {
            $qry = "SELECT * FROM `number` WHERE is_delete=1 ORDER BY `date` DESC";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }

    // get single data
    public function show() {
        try {
            $qry = "SELECT * FROM `number` WHERE `u_id` = '" . $this->id . "'";
            $q = $this->conn->query($qry) or die(mysql_error());
            $row = $q->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $row;
        return $this;
    }

    public function update() {
        if (!empty($this->name) && !empty($this->number)) {
            try {
                $upQry = "UPDATE number SET 
                        name = :name, 
                        number = :number
                        WHERE u_id = :uid";
                $q = $this->conn->prepare($upQry);
                $q->execute(array(
                            ':name' => $this->name,
                            ':number' => $this->number,
                            ':uid' => $this->id
                        )) or die('Error While Updateed');

                $_SESSION['msg'] = '<font color="green">' . "Successfully Updated..." . '</font>';
                header("location: index.php");
            } catch (Exception $ex) {
                echo 'Error: ' . $ex->getMessage();
            }
        } else {
            $_SESSION['msg'] = '<font color="red">' . "Unauthorize User" . "</font>";
            header("LOCATION: index.php");
        }
        return $this;
    }

    // trush
    public function trush() {
        echo 'number=' . $this->id;

        try {

            $upQry = "UPDATE number SET 
                        is_delete = :is_delete,
                        date = :date
                        WHERE u_id = :uid";
            $q = $this->conn->prepare($upQry);
            $q->execute(array(
                        ':is_delete' => '1',
                        ':date' => date("Y-m-d g:i:s"),
                        ':uid' => $this->id
                    )) or die('Error While Deleted');

            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }

        return $this;
    }

    // restore
    public function restore() {

        try {

            $upQry = "UPDATE number SET 
                        is_delete = :is_delete,
                        date = :date
                        WHERE u_id = :uid";
            $q = $this->conn->prepare($upQry);
            $q->execute(array(
                        ':is_delete' => '0',
                        ':date' => date("Y-m-d g:i:s"),
                        ':uid' => $this->id
                    )) or die('Error While Restored');

            $_SESSION['msg'] = '<font color="green">' . "Successfully Restored..." . '</font>';
            header("location: index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }

        return $this;
    }

    // delete form database
    public function delete() {
        try {
            $delQry = "DELETE FROM `number` WHERE u_id = :id";
            $q = $this->conn->prepare($delQry);
            $q->bindParam(':id', $this->id);
            $q->execute() or die("Error While Deleting");
            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: trushed.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

}
