<?php

include_once ('../../vendor/autoload.php');
$deleteObj = new App\MobileNumber\MNumber();

$data = $deleteObj->assign($_GET)->show();
$_SESSION['id'] = $_GET['id'];
if (isset($data) && !empty($data)) {
    $deleteObj->assign($_GET)->delete();
} else {
    $_SESSION['msg'] = '<font color="red">' . "Unauthorize User" . "</font>";
    header("location:index.php");
}
