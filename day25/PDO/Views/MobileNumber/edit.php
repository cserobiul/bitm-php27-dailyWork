<?php
include_once ('../../vendor/autoload.php');
$editObj = new App\MobileNumber\MNumber();

$data = $editObj->assign($_GET)->show();
$_SESSION['id'] = $_GET['id'];
if (isset($data) && !empty($data)) {
    ?>

    <form method="POST" action="update.php">
        <fieldset>
            <legend>Course Registration Form</legend>
            <label>Name: </label>
            <input type="text" name="name" value="<?php echo $data['name'] ?>" autofocus>

            <br>
            <label>Mobile Number</label>
            <input type="text" name="number" value="<?php echo $data['number'] ?>" autofocus>

            <br>
            <input type="submit" value="Update">
        </fieldset>
    </form>
    <?php
} else {
    $_SESSION['msg'] = '<h2><font color="red">' . "Unauthorize User" . "</font></h2>";
    header("location:index.php");
}
?>
<a href="index.php">View All</a>
