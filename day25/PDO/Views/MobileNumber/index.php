<html>
    <head>
        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            .paging-nav { text-align: right; padding-top: 2px;}
            .paging-nav a { margin: auto 1px;  text-decoration: none; display: inline-block; padding: 1px 7px;  background: #91b9e6; color: white;  border-radius: 3px;}
            .paging-nav, #tableData { width: 400px; font-family: Arial, sans-serif; }
        </style>
    </head>
    <body>
        <?php
        include_once ('../../vendor/autoload.php');
        $dataObj = new App\MobileNumber\MNumber();

        $data = $dataObj->index();
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            echo '<h2>' . $_SESSION['msg'] . '</h2>';
            unset($_SESSION['msg']);
        }
        ?>
        <h2>Current Data</h2>
        <table id="tableData" border="1" cellpadding="5">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th colspan="3">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sl = 1;
                if (isset($data) && !empty($data)) {
                    foreach ($data as $item) {
                        ?>
                        <tr>
                            <td><?php echo $sl++; ?></td>
                            <td><?php echo ucwords($item['name']); ?></td>
                            <td><?php echo ucfirst($item['number']) ?></td>
                            <td><a href="show.php?id=<?php echo $item['u_id'] ?>">View</a></td>
                            <td><a href="edit.php?id=<?php echo $item['u_id'] ?>">Edit</a></td>
                            <td><a href="trush.php?id=<?php echo $item['u_id'] ?>">Delete</a></td>
                        </tr>
                        <?php
                    }
                } else {
                    echo '<tr><td colspan="8"><font color="red">Empty Database</font></td></tr>';
                }
                ?>  
            </tbody>
        </table>
        <br>
        <a href="create.php">Add New</a>
        <a href="trushed.php">Trushed Data</a>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../js/paging.js"></script> 
        <script type="text/javascript">
            $(document).ready(function () {
                $('#tableData').paging({limit: 10});
            });
        </script>

    </body>
</html>


