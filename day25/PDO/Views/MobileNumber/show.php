<?php
include_once ('../../vendor/autoload.php');
$showObj = new App\MobileNumber\MNumber();

$data = $showObj->assign($_GET)->show();

if (isset($data) && !empty($data)) {
    ?>
    <table border="1" cellpadding="5">
        <tr>
            <th>Name</th>
            <th>Mobile Number</th>
        </tr>

        <tr>
            <td><?php echo ucwords($data['name']); ?></td>
            <td><?php echo ucfirst($data['number']); ?></td>
        </tr>


    </table>
    <?php
} else {
    $_SESSION['msg'] = '<font color="red">' . "Unauthorize User" . "</font>";
    header("location:index.php");
}
?>
<a href="index.php">View All</a>