-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2016 at 02:24 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `price` varchar(7) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `price`) VALUES
(1, 'Java Programming', 'Lang ', '260'),
(2, 'C Programming', 'June Lue', '250'),
(3, 'Php & Mysql', 'Morice Dumpar', '360'),
(4, 'C++', 'Juan Hang', '300'),
(5, 'Assembly Language', 'Jemi hao', '300');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `weaver` varchar(255) NOT NULL,
  `u_id` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `semester`, `weaver`, `u_id`) VALUES
(1, 'Masum', 'semester2', 'Yes', '578dd81513a10');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `title`, `gender`, `uid`) VALUES
(18, 'aminaa', 'male', '5774ad5f3b197'),
(19, 'amri', 'male', '5774ae234fa2c'),
(20, 'amir', 'male', '5774ae2ab2469'),
(22, 'keu na ', 'female', '5774d0cdd6b60');

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE IF NOT EXISTS `mobile` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`id`, `unique_id`, `title`) VALUES
(54, '577225583a631', 'symphony b20'),
(55, '57722571a5cb6', 'samsung s2'),
(56, '5774ae6126ffe', 'i phone s5'),
(63, '5774c17a007a7', 'keu ekjon'),
(64, '57874ff2cd217', 'sumsung 5t9'),
(65, '578750165c6d8', 'sumsung g25'),
(70, '5787627409b68', 'OR'),
(73, '578762de9b801', 'O''PI'),
(74, '578763d26f59b', 'hmm'),
(75, '578763db9de95', 'hmm2');

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE IF NOT EXISTS `mobiles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `title`, `price`) VALUES
(1, 'symphony v80', '7900'),
(2, 'samsung s2', '55000');

-- --------------------------------------------------------

--
-- Table structure for table `number`
--

CREATE TABLE IF NOT EXISTS `number` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` varchar(11) NOT NULL,
  `u_id` varchar(30) NOT NULL,
  `is_delete` int(1) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `number`
--

INSERT INTO `number` (`id`, `name`, `number`, `u_id`, `is_delete`, `date`) VALUES
(9, 'moumita', '1532', '578de9e27ef17', 1, '2016-07-19 02:07:25'),
(10, 'johir khan', '5454', '578debcce6c70', 0, '2016-07-19 10:58:00'),
(12, 'abul hosen', '01987999', '578dec8567595', 1, '2016-07-19 11:01:00'),
(13, 'amina khatuh', '054564', '578e01c59481d', 0, '2016-07-19 12:32:00'),
(14, 'kisu nai', '78788', '578e01ce84df5', 0, '2016-07-19 12:32:00'),
(15, 'masum', '4545', '578e01d52a761', 0, '2016-07-19 12:32:00'),
(16, 'ornob', '454', '578e01dd63b87', 0, '2016-07-19 12:33:00'),
(17, 'himel', '6646', '578e01e215977', 0, '2016-07-19 12:33:00'),
(18, 'badhon', '65464', '578e01e647e11', 0, '2016-07-19 12:33:00'),
(19, 'manik', '5656', '578e01ecc92e1', 0, '2016-07-19 12:33:00'),
(20, 'roton', '6656', '578e01f084d99', 0, '2016-07-19 12:33:00'),
(21, 'jamil', '6646', '578e01f759abb', 0, '2016-07-19 12:33:00'),
(22, 'nayok', '645646', '578e01fcee199', 0, '2016-07-19 12:33:00'),
(23, 'keu na', '664', '578e0202c70f8', 0, '2016-07-19 12:33:00'),
(24, 'nai', '545645', '578e04028fd87', 0, '2016-07-19 12:42:00');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE IF NOT EXISTS `terms` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `tc` varchar(10) NOT NULL,
  `uniq_id` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `name`, `tc`, `uniq_id`) VALUES
(24, 'omar faruque', '', '578a33e420130'),
(26, 'kisu nai', 'checked', '578a34143880a'),
(27, 'moumita', 'checked', '578a34467587d');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile`
--
ALTER TABLE `mobile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `number`
--
ALTER TABLE `number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `mobile`
--
ALTER TABLE `mobile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `number`
--
ALTER TABLE `number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
