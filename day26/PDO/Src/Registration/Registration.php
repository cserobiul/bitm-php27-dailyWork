<?php

namespace App\Registration;

use PDO;
use PDOException;

class Registration {

    public $id = '';
    public $username = '';
    public $email = '';
    public $pass = '';
    public $rpass = '';
    public $data = '';
    public $tc = '';
    public $user = "root";
    public $password = '';
    
    public $conn = "";
    public $ad1 = '';
    public $ad2 = '';
    public $zip = '';
    public $city = '';
    public $fname = ''; 
            public $lname, $phone, $religion, $gender, $national_id, $nationality, $bloodgroup, $webaddress;

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=php27', $this->user, $this->password);
        //$Vdate = date("Y-m-d g:i:s");
        date_default_timezone_set("Asia/Dhaka");
    }

    // assgin value
    public function assign($data) {
        if (!empty($data['username'])) {
            $this->username = $data['username'];
        } else {
            $_SESSION['username'] = '<font color="red">' . "Username Required" . '</font>';
        }

        if (!empty($data['email'])) {
            $this->email = $data['email'];
        } else {
            $_SESSION['email'] = '<font color="red">' . "Email Required" . '</font>';
        }

        if (!empty($data['pass'])) {
            $this->pass = $data['pass'];
        } else {
            $_SESSION['pass'] = '<font color="red">' . "Password Required" . '</font>';
        }

        if (!empty($data['rpass'])) {
            $this->rpass = $data['rpass'];
        } else {
            $_SESSION['rpass'] = '<font color="red">' . "Repeated Password Required" . '</font>';
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        /// profile 
        if (!empty($data['fname'])) {
            $this->fname = $data['fname'];
        }
        if (array_key_exists('lname', $data)) {
            $this->lname = $data['lname'];
        }

        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('religion', $data)) {
            $this->religion = $data['religion'];
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }

        if (array_key_exists('national_id', $data)) {
            $this->national_id = $data['national_id'];
        }
        if (array_key_exists('nationality', $data)) {
            $this->nationality = $data['nationality'];
        }

        if (array_key_exists('bloodGroup', $data)) {
            $this->bloodgroup = $data['bloodGroup'];
        }
        if (array_key_exists('webAddress', $data)) {
            $this->webaddress = $data['webAddress'];
        }

        if (array_key_exists('ad1', $data)) {
            $this->ad1 = $data['ad1'];
        }
        if (array_key_exists('ad2', $data)) {
            $this->ad2 = $data['ad2'];
        }
        if (array_key_exists('zip', $data)) {
            $this->zip = $data['zip'];
        }
        if (array_key_exists('city', $data)) {
            $this->city = $data['city'];
        }

        if (!empty($data['tc'])) {
            $this->tc = $data['tc'];
        } else {
            $this->tc = 'n';
            $_SESSION['tc'] = '<font color="red">' . "Please Check Terms & Condition" . '</font>';
        }

        return $this;
    }

    // store value into database 
    public function store() {



        if ($this->tc == 'y') {
            try {
                $query = "INSERT INTO user(username,email,pass,tc,u_id,is_active,is_admin,created_at)
                    VALUES(:username,:email,:pass,:tc, :u_id,:is_active,:is_admin, :created_at)";
                $q = $this->conn->prepare($query);
                $q->execute(array(
                    ':username' => $this->username,
                    ':email' => $this->email,
                    ':pass' => $this->pass,
                    ':tc' => $this->tc,
                    ':u_id' => uniqid(),
                    ':is_active' => '1',
                    ':is_admin' => '1',
                    ':created_at' => date("Y-m-d G:i:s")
                ));
                $rowCount = $q->rowCount();
                if ($rowCount > 0) {

                    $id = $this->conn->lastInsertId();
                    $query = "INSERT INTO profiles(user_id) VALUES(:id)";
                    $qr = $this->conn->prepare($query);
                    $qr->execute(array(
                        ':id' => $id,
                    ));
                    $row = $qr->rowCount();
                    if ($row > 0) {
                        $_SESSION['msg'] = '<font color="green">' . "Successfully data submitted" . '</font>';
                        unset($_SESSION['user']);
                        unset($_SESSION['emailvalue']);
                        header("LOCATION: index.php");
                    } else {
                        $_SESSION['msg'] = '<font color="red">' . "Error while Registration" . '</font>';
                        header("LOCATION: create.php");
                    }
                } else {
                    $_SESSION['msg'] = '<font color="red">' . "Error while Registration" . '</font>';
                    header("LOCATION: create.php");
                }
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("LOCATION: create.php");
        }
        return $this;
    }

    // get single data
    public function show() {
        try {
            $qry = "SELECT * FROM `user` WHERE `u_id` = '" . $this->id . "'";
            $q = $this->conn->query($qry) or die(mysql_error());
            $row = $q->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $row;
        return $this;
    }

    // get all data from database
    public function index() {
        try {
            $qry = "SELECT * FROM user WHERE is_delete=0";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }

    // get all data from database
    public function tIndex() {
        try {
            $qry = "SELECT * FROM `user` WHERE is_delete=1 ORDER BY `deleted_at` DESC";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }

    // trush
    public function trush() {
        echo 'number=' . $this->id;

        try {

            $upQry = "UPDATE user SET 
                        is_delete = :is_delete,
                        deleted_at = :deleted_at
                        WHERE u_id = :uid";
            $q = $this->conn->prepare($upQry);
            $q->execute(array(
                        ':is_delete' => '1',
                        ':deleted_at' => date("Y-m-d G:i:s"),
                        ':uid' => $this->id
                    )) or die('Error While Deleted');

            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }

        return $this;
    }

    // login

    public function login() {
        if (!empty($this->username) && !empty($this->pass)) {
            try {

                $qry = "SELECT * FROM `user` WHERE 
                
                    username = '" . $this->username . "' &&
                    pass = '" . $this->pass . "' &&
                        is_active = '1' ";

                $q = $this->conn->query($qry) or die(mysql_error());
                $row = $q->fetch(PDO::FETCH_ASSOC);
                $_SESSION['logMsg'] = 'Successfully login';

                return $row;
                return $this;
            } catch (Exception $ex) {
                $_SESSION['logMsg'] = 'Login First';
                echo 'Error: ' . $ex->getMessage();
            }
        } else {
            header("location:login.php");
        }
    }

    /// profiles functions ///
    public function pindex() {
        try {
            $qry = "SELECT * FROM profiles WHERE user_id = :uid";
            $q = $this->conn->prepare($qry) or die(mysql_error());
            $q->execute(array(
                ':uid' => $this->id,
            ));
            $row = $q->rowCount();
            if ($row > 0) {
                $rows = $q->fetch(PDO::FETCH_ASSOC);
                return $rows;
            } else {
                return $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }

        return $this;
    }

    // profile update
    public function proUpdate() {
        
        echo '<pre>';
        print_r($_REQUEST);
        
    
        try {
            $upQry = "UPDATE profiles SET 
                        firstName = :fn, 
                        lastName = :ln,
                        phone = :phone,
                        address = :address,
                        religion = :religion,
                        gender = :gender,
                        national_id = :national_id,
                        nationality = :nationality,
                        bloodGroup = :bloodGroup,
                        webAddress = :webAddress
                        WHERE user_id = :uid";
            $q = $this->conn->prepare($upQry);
            $address = array(
                'ad1' => $this->ad1,
                'ad2' => $this->ad2,
                'zip' => $this->zip,
                'city' => $this->city
            );
            $serializeAddress = serialize($address);
            $q->execute(array(
                ':fn' => $this->fname,
                ':ln' => $this->lname,
                ':phone' => $this->phone,
                ':address' => $serializeAddress,
                ':religion' => $this->religion,
                ':gender' => $this->gender,
                ':national_id' => $this->national_id,
                ':nationality' => $this->nationality,
                ':bloodGroup' => $this->bloodgroup,
                ':webAddress' => $this->webaddress,
                ':uid' => $this->id
            ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $_SESSION['msg'] = '<font color="green">' . "Successfully Updated..." . '</font>';
                header("location: profiles.php");
            } else {
                $_SESSION['msg'] = '<font color="green">' . "Error while Updated..." . '</font>';
                header("location: index.php");
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

}
