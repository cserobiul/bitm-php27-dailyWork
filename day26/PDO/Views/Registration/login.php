<?php
session_start();
?>


<form method="POST" action="loginProcess.php">
    <fieldset>
        <legend>Login Form</legend>
        
        <?php 
            if(isset($_SESSION['logMsg']) && !empty($_SESSION['logMsg'])){
                echo '<h3><font color="red">'.$_SESSION['logMsg'].'</font></h3>';
                unset($_SESSION['logMsg']);
            }
        ?>
        
        
        <label>Username: </label>
        <input type="text" name="username" autofocus>
        <?php 
            if(isset($_SESSION['username']) && !empty($_SESSION['username'])){
                echo $_SESSION['username'];
                unset($_SESSION['username']);
            }
        ?>
        <br>
        <label>Password</label>
        <input type="password" name="pass">
        <?php 
            if(isset($_SESSION['pass']) && !empty($_SESSION['pass'])){
                echo $_SESSION['pass'];
                unset($_SESSION['pass']);
            }
        ?>
        <br>
        <input type="submit" value="Login">
    </fieldset>
</form>

