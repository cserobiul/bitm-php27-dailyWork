<?php
include_once ('../../vendor/autoload.php');

$loginObj = new App\Registration\Registration();

if($_SERVER['REQUEST_METHOD'] == "POST"){   
    
$data = $loginObj->assign($_POST)->login();

if(isset($data) && !empty($data)){
    $_SESSION['loginUser'] = $data['username'];
    $_SESSION['is_admin'] = $data['is_admin'];
    $_SESSION['id'] = $data['id'];
    
    header('location:index.php');
    
}else{
    $_SESSION['logMsg'] = 'You Type Worng information';
    header('location:login.php');
}
    
    
    
}else{
    
    $_SESSION['logMsg'] = 'Type Username and Password First';
    header('location:login.php');
}

