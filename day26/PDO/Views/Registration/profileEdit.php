<?php
include_once ('../../vendor/autoload.php');

$dataObj = new App\Registration\Registration();

if (isset($_SESSION['loginUser']) && !empty($_SESSION['loginUser'])) {
    echo 'Dear '.  ucfirst($_SESSION['loginUser']).', You '.$_SESSION['logMsg'];
    
    echo ' <a href="logout.php">Logout</a>';

$data = $dataObj->assign($_SESSION)->pindex();

?>
<h2>User Edit Page</h2>
<?php if (!empty($data)) { 
$address = $data['address'];
$address=  unserialize($address);



?>

<form method="POST" action="profileUpdate.php">
    <table border="1" cellpadding="5">
        <tr>
            <td>First Name: </td>
            <td><input name="fname" value="<?php echo $data['firstName']; ?>"></td>                        
        </tr>
        <tr>
            <td>Last Name: </td>
            <td><input name="lname" value="<?php echo $data['lastName']; ?>"></td>            
        </tr>
        <tr>
            <td>Phone: </td>
            <td><input name="phone" value="<?php echo $data['phone']; ?>"></td>
        </tr>
        <tr>
            <td>Address Line1: </td>
            <td><input name="ad1" value="<?php echo $address['ad1']; ?>"></td>
        </tr>
        <tr>
            <td>Address Line2: </td>
           <td><input name="ad2" value="<?php echo $address['ad2']; ?>"></td>
        </tr>
        <tr>
            <td>Zip Code: </td>
            <td><input name="zip" value="<?php echo $address['zip']; ?>"></td>
        </tr>
        <tr>
            <td>City: </td>
            <td><input name="city" value="<?php echo $address['city']; ?>"></td>
        </tr>
        <tr>
            <td>Religion: </td>
            <td><input name="religion" value="<?php echo $data['religion']; ?>"></td>
        </tr>
        <tr>
            <td>Gender: </td>
            <td><input name="gender" value="<?php echo $data['gender']; ?>"></td>
        </tr>
        <tr>
            <td>National Id: </td>
            <td><input name="national_id" value="<?php echo $data['national_id']; ?>"></td>
        </tr>
        <tr>
            <td>Nationality: </td>
            <td><input name="nationality" value="<?php echo $data['nationality']; ?>"></td>
        </tr>
        <tr>
            <td>Blood Group: </td>
            <td><input name="bloodGroup" value="<?php echo $data['bloodGroup']; ?>"></td>
        </tr>
        <tr>
            <td>Web Address: </td>
            <td><input name="webAddress" value="<?php echo $data['webAddress']; ?>"></td>
        </tr>
        <tr>
            <td></td>
            <td><input value="Update" type="submit"></td>
        </tr>


    </table>
</form>
<?php }else{
    echo 'No Record';
}
?>


<br>

<?php

    echo '<a href="index.php">Dashboard</a> ||';
    echo '<a href="profileEdit.php">Edit</a>';

?>

<?php }else{
    $_SESSION['logMsg'] = 'Login First';
    header('location:login.php');
}
?>