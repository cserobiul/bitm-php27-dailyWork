<?php
include_once ('../../vendor/autoload.php');

$dataObj = new App\Registration\Registration();

if (isset($_SESSION['loginUser']) && !empty($_SESSION['loginUser'])) {
    echo 'Dear '.  ucfirst($_SESSION['loginUser']).', You '.$_SESSION['logMsg'];
    
    echo ' <a href="logout.php">Logout</a>';

$data = $dataObj->assign($_SESSION)->pindex();
$address = $data['address'];
$address=  unserialize($address);

if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo '<h2>' . $_SESSION['msg'] . '</h2>';
    unset($_SESSION['msg']);
}
?>
<h2>User Details</h2>
<?php if (!empty($data)) {  ?>
    <table border="1" cellpadding="5">
        <tr>
            <td>Full Name: </td>
            <td><?php echo ucwords($data['firstName']). ' ' .ucwords($data['lastName']); ?>
            </td>            
        </tr>
        <tr>
            <td>Phone: </td>
            <td><?php echo ucfirst($data['phone']); ?></td>
        </tr>
        <tr>
            <td>Address: </td>
            <td><?php echo ucfirst($address['ad1']).", ".ucfirst($address['ad2']) ; ?></td>
        </tr>
        <tr>
            <td>Zip: </td>
            <td><?php echo ucfirst($address['zip']); ?></td>
        </tr>
        <tr>
            <td>City: </td>
            <td><?php echo ucfirst($address['city']); ?></td>
        </tr>
        <tr>
            <td>Religion: </td>
            <td><?php echo ucfirst($data['religion']); ?></td>
        </tr>
        <tr>
            <td>Gender: </td>
            <td><?php echo ucfirst($data['gender']); ?></td>
        </tr>
        <tr>
            <td>National Id: </td>
            <td><?php echo ucfirst($data['national_id']); ?></td>
        </tr>
        <tr>
            <td>Nationality: </td>
            <td><?php echo ucfirst($data['nationality']); ?></td>
        </tr>
        <tr>
            <td>Blood Group: </td>
            <td><?php echo ucfirst($data['bloodGroup']); ?></td>
        </tr>
        <tr>
            <td>Web Address: </td>
            <td><?php echo ucfirst($data['webAddress']); ?></td>
        </tr>


    </table>
<?php }else{
    echo 'No Record';
}
?>


<br>

<?php

    echo '<a href="index.php">Dashboard</a> || ';
    echo '<a href="profileEdit.php">Edit</a>';

?>

<?php }else{
    $_SESSION['logMsg'] = 'Login First';
    header('location:login.php');
}
?>