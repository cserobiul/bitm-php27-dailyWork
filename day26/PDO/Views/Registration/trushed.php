<?php
include_once ('../../vendor/autoload.php');

$dataObj = new App\Registration\Registration();

$data = $dataObj->tIndex();
//echo "<pre>";
//print_r($data);
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo '<h2>' . $_SESSION['msg'] . '</h2>';
    unset($_SESSION['msg']);
}
?>
<h2>Trushed Data</h2>
<table border="1" cellpadding="5">
    <thead>
        <tr>
            <th>SL</th>
            <th>Name</th>
            <th>Mobile Number</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sl = 1;
        if (isset($data) && !empty($data)) {
            foreach ($data as $item) {
                ?>
                <tr>
                    <td><?php echo $sl++; ?></td>
                    <td><?php echo ucwords($item['username']); ?></td>
                    <td><?php echo ucfirst($item['email']) ?></td>

                    <td><a href="restore.php?id=<?php echo $item['u_id'] ?>">Restore</a></td>
                    <td><a href="delete.php?id=<?php echo $item['u_id'] ?>">Permanent Delete</a></td>


                </tr>
                <?php
            }
        } else {
            echo '<tr><td colspan="8"><font color="red">Empty Database</font></td></tr>';
        }
        ?>  

    </tbody>
</table>


<br>
<a href="create.php">Add New</a> ||
<a href="index.php">Active User</a>




