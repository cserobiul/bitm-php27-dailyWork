<!doctype html>
<html>
    <head>
        <title>Country :: HOME</title>      
    </head>
    <body>
        <h2>Country List</h2>
        <table border="1" style="width:60%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Country Name</th>
                    <th>Short Name</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody id="data-load-here">

            </tbody>
        </table>

        <h2 id="addCountry">Add Country</h2>
        <h2 id="viewCountry">View and Update Country</h2>
        <form name="dataForm">
            <table>
                <tr>
                    <td>ID:</td>
                    <td><input type="text" id="id" name="id" disabled></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" id="cname" name="cname"></td>
                </tr>
                <tr>
                    <td>Short Name:</td>
                    <td><input type="text" id="sname" name="sname"></td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="button" onclick="insertData()" value="Insert Data" id="submit">
                        <input type="button" onclick="updateData()" value="Update Data" id="update">
                    </td>
                </tr>
            </table>
        </form>
        <div id="errMsg"></div>
        <script type="text/javascript" src="scripts/jquery-3.1.0.min.js"></script>
        <script type="text/javascript">

                            loadData();

                            $(document).on("click", ".selectData", function () {
                                var id = $(this).attr("id");
                                console.log(id);

                                $.ajax({
                                    type: "POST",
                                    data: "id=" + id,
                                    url: "show.php",
                                    success: function (result) {
                                        var resultObj = JSON.parse(result);
                                        $("#id").val(resultObj.id);
                                        $("#cname").val(resultObj.name);
                                        $("#sname").val(resultObj.shortName);

                                        $("#update").show();
                                        $("#submit").hide();
                                        $("#viewCountry").show();
                                        $("#addCountry").hide();

                                        console.log(resultObj);
                                    }
                                })


                            });

                            $(document).on("click", ".deleteData", function () {

                                var id = $(this).attr("id");
                                console.log(id);

                                $.ajax({
                                    type: "POST",
                                    data: "id=" + id,
                                    url: "delete.php",
                                    success: function (result) {
                                        var resultObj = JSON.parse(result);
                                        $("#errMsg").html(resultObj.msg);
                                        $("#errMsg").append("<br><a href='index.php'>View All</a>");
                                        console.log(resultObj);
                                        loadData();
                                    }
                                })
                            })

                            function deleteData() {
                                var id = $("#id").val();

                                $.ajax({
                                    type: "POST",
                                    data: "id=" + id,
                                    url: "delete.php",
                                    success: function (result) {
                                        var resultObj = JSON.parse(result);
                                        $("#errMsg").html(resultObj.msg);
                                        $("#errMsg").append("<br><a href='index.php'>View All</a>");
                                        loadData();
                                        console.log(resultObj);
                                    }
                                });
                            }

                            function updateData() {
                                var id = $("#id").val();
                                var cName = $("#cname").val();
                                var sName = $("#sname").val();

                                $.ajax({
                                    type: "POST",
                                    //data: $("#dataForm").serialize(),
                                    data: "id=" + id + "&cname=" + cName + "&sname=" + sName,
                                    url: "update.php",
                                    success: function (result) {
                                        var resultObj = JSON.parse(result);
                                        $("#errMsg").html(resultObj.msg);
                                        $("#errMsg").append("<br><a href='index.php'>View All</a>");
                                       
                                        loadData();
                                        //console.log(resultObj);
                                    }
                                });


                            }

                            function insertData() {
                                //alert('data submitted');
                                var cName = $("#cname").val();
                                var sName = $("#sname").val();

                                $.ajax({
                                    type: "POST",
                                    //data: $("#dataForm").serialize(),
                                    data: "cname=" + cName + "&sname=" + sName,
                                    url: "process.php",
                                    success: function (result) {
                                        var resultObj = JSON.parse(result);
                                        $("#errMsg").html(resultObj.msg);
                                        loadData();
                                        //console.log(result);
                                    }
                                });


                            }
                            function loadData() {
                                var dataHandler = $("#data-load-here");
                                dataHandler.html(" ");
                                $.ajax({
                                    type: "get",
                                    data: "",
                                    url: "getData.php",
                                    success: function (result) {
                                        var resultObj = JSON.parse(result);

                                        $.each(resultObj, function (index, value) {
                                            var newRow = $("<tr>");
                                            newRow.html("<td>" + value.id + "</td><td>" + value.name + "</td><td>"
                                                    + value.shortName + "</td>"
                                                    + "<td><button class='selectData' id='" + value.id + "'>View</button></td> <td><button class='deleteData' id='" + value.id + "'>Delete</button></td>");
                                            dataHandler.append(newRow);
                                            $("#update").hide();
                                            $("#viewCountry").hide();
                                            //console.log(resultObj);
                                        });
                                    }
                                });
                            }

        </script>
    </body>
</html>