<html>
    <head>
        <title>Ajax Testing</title>
        <script src="jquery-3.1.0.min.js"></script>
    </head>
    <body>


        <form id="submit_form" method="post">
            Country Name:<input name="cname" type="text" id="cname"><br>
            Short Name: <input name="sname" type="text" id="sname"></br>
            <input type="button" id="submit" value="Send Data">
        </form>
        <div id="response">
            
        </div>

        <script>

            $(document).ready(function () {
                $('#submit').click(function () {
                    var cName = $('#cname').val();
                    var sName = $('#sname').val();
                    
                    if(cName == '' || sName == ''){
                        $('#response').html('Both Field are required...');
                    }else{
                        $.ajax({
                           url:"process.php", 
                           method:"POST",
                           data:$('#submit_form').serialize(),
                        }).done(function(response){
                            $('#response').html(response);
                            $('#submit_form').hide();
                        });
                    }
                    
                    

                });
            });
        </script>
    </body>     
</html>