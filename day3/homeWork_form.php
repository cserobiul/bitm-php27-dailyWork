<html>
    <head>
        <title>Simple HTML Form</title>
    </head>
    <body>
        <form method="post" action="#">
            <fieldset>
                <legend>Registration Form</legend>
                
                <fieldset>
                    <legend>Personal Information</legend>
                    First Name: <br>
                    <input name="fname" type="text" minlength="2" maxlength="20" required autofocus><br>
                    Middle Name: <br>
                    <input name="mname" type="text" minlength="3" maxlength="20" placeholder="Optional"><br>
                    Last Name: <br>
                    <input name="lname" type="text" minlength="3" maxlength="20" required><br>
                    Gender: 
                    Male <input name="gender" type="radio" value="male" required>
                    Female
                    <input name="gender" type="radio" value="female"><br>
                    Date of Birth:
                    <input type="date" name="dob" min="1950-01-01"max="2016-05-24" required>
                </fieldset>
                
                <fieldset>
                    <legend>Contact Information</legend>
                     Address Line1: <br>
                    <input name="address1" type="text" required><br>
                     Address Line2: <br>
                    <input name="address2" type="text" placeholder="Optional"><br>
                     Zip Code: <br>
                     <input name="zip" type="text" required><br>
                     City: <br>
                     <select name="city" required>
                         <option value="comilla">Comilla</option>
                         <option value="mirpur" selected>Dhaka</option>
                         <option value="faridpur">Faridpur</option>                         
                         <option value="sherpur">Sherpur</option>
                         <option value="manikgong">Manikgong</option>
                     </select>
                     <br>
                     Email: <br>
                     <input name="email" type="email" required><br>
                      Phone: <br>
                      <input name="phone" type="text" minlength="11" maxlength="11" required><br>
                </fieldset>
                
                <fieldset>
                    <legend>Login Information</legend>
                    Username: <br>
                    <input name="username" type="text" minlength="8" maxlength="25" required><br>
                      Password: <br>
                      <input name="pass" type="password" minlength="8" maxlength="12" placeholder="length 8 to 12" required><br>
                    Repeate Password: <br>
                    <input name="rpass" type="password" minlength="8" maxlength="12" placeholder="length 8 to 12" required><br><br>
                    
                    <input nmae="reset" type="reset" value="Clear">
                    <input nmae="reg" type="submit" value="Registration">
                </fieldset>
                    
                
                
            </fieldset>
        </form>
    </body>
</html>

