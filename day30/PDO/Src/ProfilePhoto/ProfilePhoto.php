<?php 


namespace Apps\ProfilePhoto;
use Apps\ProfilePhoto\Message;
use Apps\ProfilePhoto\Utility;
use PDO;
use PDOException;

class ProfilePhoto {
	public $id,$image,$name,$conn,$data;
	
	public function __construct(){
		session_start();
		try{
			$this->conn = new PDO('mysql:host=localhost;dbname=php27','root','');
			//$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERROMDE_EXCEPTION);
		}catch (PDOException $e){
			echo 'Error: '.$e->getMessage();
		}
	}

	public function assign($data = ''){
		if(is_array($data) && array_key_exists('name', $data)){
			$this->name = $data['name'];
		}
		if(is_array($data) && array_key_exists('img', $data)){
			$this->image = $data['img'];
		}
		if(array_key_exists('id', $data) && !empty($data['id'])){
			$this->id = $data['id'];
		}

		return  $this;
	}

	public function store(){
		if(!empty($this->name) || !empty($this->image)){
			$qry = "INSERT INTO profilephoto (name,image) VALUES(:n,:img) ";
			$q = $this->conn->prepare($qry);
			$q->execute(array(
				':n' =>$this->name,
				':img' =>$this->image,
				));
			if($q){
				Message::message("Data has been inserted successfully");
				Utility::redirect();
			}else{
				Message::message("Data error");
				header('Location: creater.php');
			}
		}
	}

	public  function index(){
		$qry = "SELECT * FROM profilephoto";
		$q = $this->conn->query($qry) or die("Failed !");
		while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
			$this->data[] = $row;
		}
		return $this->data;
	}

	public function show(){
		$qry = "SELECT * FROM profilephoto WHERE id = :id";
		$q = $this->conn->prepare($qry) or die("Failed");
		$q->execute(array(
			':id' => $this->id,
			));
		$singleData = $q->fetch(PDO::FETCH_ASSOC);
		return $singleData;
	}

	// update info

	public function update(){
		if(!empty($this->image)){
			$qry = "UPDATE profilephoto SET 
			name = :n, image = :i WHERE id = :id ";
			$q = $this->conn->prepare($qry) or die("Failed");
			$q->execute(array(
				':n'=>$this->name,
				':i'=>$this->image,
				':id'=>$this->id,
				));
		}else{
			$qry = "UPDATE profilephoto SET 
			name = :n WHERE id = :id ";
			$q = $this->conn->prepare($qry) or die("Failed");
			$q->execute(array(
				':n'=>$this->name,
				':id'=>$this->id,
				));
		}
		if($q){
			Message::message("Profile data has been updated successfully");
			Utility::redirect();
		}else{
			Message::message("Error while updated");
		}
	}

	public function delete(){
		$qry = "DELETE FROM profilephoto WHERE id = :id";
		$q = $this->conn->prepare($qry) or die("Error");
		$q->execute(array(
			':id' =>$this->id,
			));
		if($q){
			Message::message("Profile data has been deleted successfully");
			Utility::redirect();
		}else{
			Message::message("Error While data deleted");
		}
	}
}

?>