<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Create :: User</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form action="store.php" method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>Create New User</legend>
			<label>User Name: </label>
			<input type="text" name="name"> <br>
			<label>User Photo: </label>
                        <img id="photo" src="">
			<input type="file" name="photo" onchange="readURL(this);" ><br>
			<input type="submit" value="Save">

		</fieldset>
	</form>
	<h4><a href="index.php">All Data</a></h4>
        <script src="jquery.js"></script>
        <script type="text/javascript">
                function readURL(input){
                        if(input.files && input.files[0]){
                                var reader = new FileReader();

                                reader.onload=function(e){
                                        $('#photo').attr('src',e.target.result).width(100).height(100);
                                };
                                reader.readAsDataURL(input.files[0]);
                        }
                }
        </script>
</body>
</html>