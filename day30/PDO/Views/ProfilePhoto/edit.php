<?php 
include_once ('../../vendor/autoload.php');
$showObj = new Apps\ProfilePhoto\ProfilePhoto();
$_SESSION['id'] = $_REQUEST['id'];
$data = $showObj->assign($_REQUEST)->show();

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Create :: User</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form action="update.php" method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>Update User</legend>
			<label>User Name: </label>
			<input type="text" name="name" value="<?php echo $data['name']; ?>"> <br>
			<label>User Photo: </label>
			
			<img id="photo" src="<?php echo '../../img/'.$data['image']; ?>" alt="No Image" height="100px" width="100px">
			<input type="file" name="photo" value="<?php echo $data['image'] ?>" onchange="readURL(this);">

			<br>

			<input type="submit" value="Update">

		</fieldset>
	</form>
	<h4><a href="index.php">All Data</a></h4>
        <script src="jquery.js"></script>
        <script type="text/javascript">
                function readURL(input){
                        if(input.files && input.files[0]){
                                var reader = new FileReader();

                                reader.onload=function(e){
                                        $('#photo').attr('src',e.target.result).width(100).height(100);
                                };
                                reader.readAsDataURL(input.files[0]);
                        }
                }
        </script>
</body>
</html>