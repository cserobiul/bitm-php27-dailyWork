<?php 
include_once ('../../vendor/autoload.php');
$ppObj = new Apps\ProfilePhoto\ProfilePhoto();
use Apps\ProfilePhoto\Message;
use Apps\ProfilePhoto\Utility;


$data = $ppObj->index();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Home Page</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h2>User List</h2>	
	<div class="warning">
		<?php if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
			echo Message::message();
		} ?>
	</div>
	<?php if(!empty($data)){ ?>
	<table border="1" cellpadding="10">
		<thead>
			<tr>
				<th>SL</th>
				<th>Name</th>
				<th colspan="3">Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php $sl=1;
			foreach ($data as $item) { ?>
			<tr>
				<td><?php echo $sl++; ?> </td>
				<td><?php echo $item['name'] ?> </td>				
				<td><a href="show.php?id=<?php echo $item['id']; ?>">View</a></td>
					<td><a href="edit.php?id=<?php echo $item['id']; ?>">Edit</a></td>
					<td><a href="delete.php?id=<?php echo $item['id']; ?>">Delete</a></td> 
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php }else{
			echo "<h3>Empty database</h3>";
		} ?>
		<h4><a href="create.php">Add More</a></h4>
	</body>
	</html>