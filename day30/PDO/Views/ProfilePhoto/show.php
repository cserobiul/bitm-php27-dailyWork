<?php 
include_once ('../../vendor/autoload.php');
$showObj = new Apps\ProfilePhoto\ProfilePhoto();
//$_REQUEST
$data = $showObj->assign($_REQUEST)->show();

 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title>Show Single Data</title>
 	<link rel="stylesheet" href="">
 </head>
 <body>
 	<table border="1" cellpadding="10">
 		<thead>
 			<tr>
 				<th>ID</th>
 				<th>Name</th>
 				<th>Profile Photo</th>
 			</tr>
 		</thead>
 		<tbody>
 			<tr>
 				<td><?php echo $data['id']; ?> </td>
 				<td><?php echo $data['name']; ?> </td>
 				<td> <img src="<?php echo '../../img/'.$data['image']; ?>" alt="No Images" height="100" width="100"></td>								
 			</tr>
 		</tbody>
 	</table>
 	<h4><a href="index.php">All Data</a></h4>
 </body>
 </html>