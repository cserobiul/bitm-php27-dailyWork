<?php 
include_once ('../../vendor/autoload.php');
$showObj = new Apps\ProfilePhoto\ProfilePhoto();

$data = $showObj->assign($_SESSION)->show();
$_POST['id']=$_SESSION['id'];
// print_r($data);
// die();
if(isset($_FILES['photo'])){
	$errors = array();
	$filename = time().$_FILES['photo']['name'];
	$filetype = $_FILES['photo']['type'];
	$filetemp = $_FILES['photo']['tmp_name'];
	$filesize = $_FILES['photo']['size'];
	$fileext = strtolower(end(explode('.', $_FILES['photo']['name'])));

	$formats = array("jpeg","jpg","png");

	if(in_array($fileext, $formats) === false){
		$errors[]="Extension not allowed";
	}

	if($filesize>2097152){
		$errors[]="File size must be excately 2 MB";
	}

	if(empty($errors) == true){
		unlink($_SERVER['DOCUMENT_ROOT'].'/bitm-php27-dailyWork/day30/PDO/img/'.$data['image']);
		move_uploaded_file($filetemp, "../../img/".$filename);
		$_POST['img']=$filename;
	}else{
		print_r($errors);
	}
}

$showObj->assign($_POST)->update();
 ?>