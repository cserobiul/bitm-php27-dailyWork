<html>
    <head>
        <title>Image Upload</title>
    </head>
    <body>
        <fieldset>
            <legend>Image Upload</legend>
            <?php
            session_start();
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
            ?>
            <form action="store.php" method="post" enctype="multipart/form-data">
                Your Name: <input type="text" name="name"> <br>
                Select image to upload:
                <input type="file" name="fileToUpload" id="fileToUpload"><br>
                <input type="submit" value="Save" name="submit">
            </form>
        </fieldset>
    </body>
</html>
