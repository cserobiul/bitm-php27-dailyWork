<?php 
namespace Apps\PhotoSlider;
use Apps\PhotoSlider\Utility;
use Apps\PhotoSlider\Message;
use PDO;
use PDOException;

class PhotoSlider {

	public $id, $pname,$description,$image,$data,$conn;
	public function __construct()
	{
		session_start();
		try{
			$this->conn = new PDO('mysql:host=localhost;dbname=php27','root','');
			//$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERROMDE_EXCEPTION);
		}catch (PDOException $e){
			echo 'Error: '.$e->getMessage();
		}
	}

	// data assign into variable

	public function assign($data=''){
		if(is_array($data) && array_key_exists('pname', $data)){
			$this->pname = $data['pname'];
		}
		if(is_array($data) && array_key_exists('description', $data)){
			$this->description = $data['description'];
		}
		if(is_array($data) && array_key_exists('img', $data)){
			$this->image = $data['img'];
		}
		if(array_key_exists('id', $data) && !empty($data['id'])){
			$this->id = $data['id'];
		}

		return  $this;
	}

// data store into database
	public function store(){
		if(!empty($this->pname) || !empty($this->description) || !empty($this->image)){
			try{
				$qry = "INSERT INTO photoSlider (pname,description,image) VALUES(:n,:d,:img) ";
				$q = $this->conn->prepare($qry);
				$q->execute(array(
					':n' =>$this->pname,
					':d' =>$this->description,
					':img' =>$this->image,
					));
				if($q){
					Message::message("Data has been inserted successfully");
					Utility::redirect();
				}else{
					Message::message("Data error");
					header('Location: create.php');
				}
			}catch(PDOException $e){
				echo 'Error: '. $e->getMessage;
			}
		}
		return $this;
	}

	public function index(){
		try{
			$qry = "SELECT * FROM photoSlider ORDER BY id DESC LIMIT 5";
			$q = $this->conn->query($qry) or die("Failed !");
			$rows = $q->rowCount();
			if($rows>0){
				while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
					$this->data[] = $row;
				}
				return $this->data;
			}else{
				return $rows;
			}					
		}catch(PDOException $e){
			echo 'Error: '. $e->getMessage;
		}
		return $this;
	}

}

?>