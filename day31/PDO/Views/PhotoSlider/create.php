<?php 
session_start();
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Slider</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<script src="..js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</head>
<body>
	<br>
	<div class="container-fluid">
		<div class="row-fluid col-md-6 col-md-offset-3">
		<h2>Add a New Photo </h2>
			<form action="store.php" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="exampleInputEmail1">Photo Name</label>
					<input type="text" name="pname" class="form-control" id="pname" placeholder="Enter Photo Name">
					<?php if(isset($_SESSION['nameErrMsg']) && !empty($_SESSION['nameErrMsg'])){
						echo '<p style="color:red">'.$_SESSION['nameErrMsg'].'</p>';
						unset($_SESSION['nameErrMsg']);
						} ?>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Photo Description</label>
					<textarea name="description" rows="5" type="text" class="form-control" id="description" placeholder="Enter Photo Description"></textarea>
					<?php if(isset($_SESSION['desErrMsg']) && !empty($_SESSION['desErrMsg'])){
						echo '<p style="color:red">'.$_SESSION['desErrMsg'].'</p>';
						unset($_SESSION['desErrMsg']);
						} ?>
				</div>
				<div class="form-group">
					<label for="exampleInputFile">Select Photo</label>
					<input name="photo" type="file" id="exampleInputFile">
					<p class="help-block">Allow: JPEG,JPG and PNG.</p>
					<?php if(isset($_SESSION['photoErrMsg']) && !empty($_SESSION['photoErrMsg'])){
						echo '<p style="color:red">'.$_SESSION['photoErrMsg'].'</p>';
						unset($_SESSION['photoErrMsg']);
						} ?>
				</div>
				<button type="submit" class="btn btn-success">Add Photo</button>
			</form>
		</div>		
	</div>
</body>
</html>

