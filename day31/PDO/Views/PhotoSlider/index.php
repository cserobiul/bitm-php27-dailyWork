<?php 
include_once ('../../vendor/autoload.php');
use Apps\PhotoSlider\PhotoSlider;
use Apps\PhotoSlider\Utility;
use Apps\PhotoSlider\Message;
$dataObj = new PhotoSlider();

$data = $dataObj->index();



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Slider</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container-fluid">
		<br>
		<div class="col-md-offset-2 col-md-8">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					<li data-target="#carousel-example-generic" data-slide-to="3"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="../../img/noimage.png" alt="No Image">
						<div class="carousel-caption">
						kisu ekta
						</div>
					</div>

					<?php 
					if (!empty($data)) {
						foreach ($data as $item) { ?>
							<div class="item">	
								<img  src="<?php echo '../../img/'.$item['image']; ?>" alt="No Images" >	
								<div class="carousel-caption">
									<h3><?php echo $item['pname']; ?></h3>
									<p><?php echo $item['description']; ?></p>
								</div>
							</div>


							<?php }
						}else{
							echo '<h2>Empty Database</h2>';
						} ?>
				</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>


			<h4><a href="create.php">Add New Photo</a></h4>
			<h4>
				<?php if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
					echo Message::message();
				}  ?>
			</h4>
		</div>

	</body>
	</html>

