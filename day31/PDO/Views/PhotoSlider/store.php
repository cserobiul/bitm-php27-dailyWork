<?php 
include_once ('../../vendor/autoload.php');
use Apps\PhotoSlider\PhotoSlider;
$storeObj = new PhotoSlider();


// echo "<pre>";
//  print_r($_POST);
//  print_r($_FILES);
if(isset($_FILES['photo'])){

	$errors = array();

	if(empty($_REQUEST['pname'])){
		$errors[] ='Photo Name Required...';
		$_SESSION['nameErrMsg'] = 'Photo Name Required...';
	}

	if(empty($_REQUEST['description'])){
		$errors[] ='Photo Description Required...';
		$_SESSION['desErrMsg'] = 'Photo Description Required...';
	}

	$filename = time().$_FILES['photo']['name'];
	$filetype = $_FILES['photo']['type'];
	$filetemp = $_FILES['photo']['tmp_name'];
	$filesize = $_FILES['photo']['size'];
	$fileext = strtolower(end(explode('.', $_FILES['photo']['name'])));

	$formats = array("jpeg","jpg","png");

	if(in_array($fileext, $formats) === false){
		$errors[]="Photo Extension not allowed";
		$_SESSION['photoErrMsg'] = 'Photo Extension not allowed...';		
	}

	if($filesize>2097152){
		$errors[]="File size must be excately 2 MB";
		$_SESSION['photoErrMsg'] = 'File size must be excately 2 MB...';
	}

	if(empty($errors) == true){
		move_uploaded_file($filetemp, "../../img/".$filename);
		$_POST['img']=$filename;
	}else{
		header('Location: create.php');
		foreach ($errors as $key) {
			echo '<p style="color:red;">'.$key.'</p>';
		}
		
	}
}

$storeObj->assign($_POST)->store();

 ?>