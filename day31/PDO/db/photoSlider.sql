-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2016 at 08:36 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `photoSlider`
--

CREATE TABLE IF NOT EXISTS `photoslider` (
  `id` int(11) NOT NULL,
  `pname` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photoSlider`
--

INSERT INTO `photoSlider` (`id`, `pname`, `description`, `image`) VALUES
(7, 'blood one', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem velit possimus architecto libero totam autem neque tenetur animi, nesciunt quaerat.', '14702918091.jpg'),
(8, 'blood 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem velit possimus architecto libero totam autem neque tenetur animi, nesciunt quaerat.', '14702919592.jpg'),
(9, 'blood 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem velit possimus architecto libero totam autem neque tenetur animi, nesciunt quaerat.', '14702920503.jpg'),
(10, 'blood 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem velit possimus architecto libero totam autem neque tenetur animi, nesciunt quaerat.', '14702924664.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `photoSlider`
--
ALTER TABLE `photoSlider`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `photoSlider`
--
ALTER TABLE `photoSlider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
