<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6bac150499a267d375bfef40a5dbe231
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Apps\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Apps\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit6bac150499a267d375bfef40a5dbe231::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit6bac150499a267d375bfef40a5dbe231::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
