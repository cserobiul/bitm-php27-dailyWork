<?php

namespace Apps\News;

use PDO;
use PDOException;

class News {

    public $id, $title, $shortDes, $details, $cat_id, $cat_name,$image,$tags, $created_at, $modified_at, $deleted_at, $data;
    public $conn;

    public function __construct() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=php27', 'root', '');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    // assgin value
    public function assign($data) {

        // category info
        if (!empty($data['cat_name'])) {
            $this->cat_name = $data['cat_name'];
        } else {
            $_SESSION['catMsg'] = '<font color="red">' . "Category Name is Required" . '</font>';

        }

        // news info start
        if (!empty($data['newsTitle'])) {
            $this->title = $data['newsTitle'];
        }

        if (!empty($data['shotrDes'])) {
            $this->shotrDes = $data['shotrDes'];
        }

        if (!empty($data['details'])) {
            $this->details = $data['details'];
        }

        if (!empty($data['cat_id'])) {
            $this->cat_id = $data['cat_id'];
        }

        if (!empty($data['img'])) {
            $this->image = $data['img'];
        }

        if (!empty($data['newsTag'])) {
            $this->tags = $data['newsTag'];
        }
        // news info end

        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        return $this;
    }

    // get category name
    public function categoryName(){
        try {
            $Query = "SELECT * FROM tbl_category WHERE id = :id";
            $q = $this->conn->prepare($Query) or die('Unable to Query');
            $q->execute(array(
                ':id' => $this->cat_id,
                ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $data = $q->fetch(PDO::FETCH_ASSOC);
                return $data;
            }else{
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    // get news by category
    public function catIntex(){    
       try {
            $Query = "SELECT * FROM tbl_news WHERE cat_id = :id ORDER BY created_at DESC ";
            $q = $this->conn->prepare($Query);
            $q->execute(array(
                ':id' =>$this->cat_id,
                ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $data[] = $row;
                }
                return $data;
            } else {
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    // get news by tags
    public function tagIntex(){    
       try {
            $Query = "SELECT * FROM tbl_news WHERE tags LIKE ? ORDER BY created_at DESC";
            $q = $this->conn->prepare($Query);
            $q->execute(array("%$this->tags%"));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $data[] = $row;
                }
                return $data;
            } else {
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    // get related news
    
    public function relatedNews(){
        try {
            $Query = "SELECT * FROM tbl_news WHERE cat_id = :cat_id ORDER BY RAND() LIMIT 4";
            $q = $this->conn->prepare($Query);
            $q->execute(array(
                ':cat_id' => $this->cat_id,
                ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $data[] = $row;
                }
                return $data;
            } else {
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }



    // get single news    
    public function show(){
        try {
            $Query = "SELECT * FROM tbl_news WHERE id = :id";
            $q = $this->conn->prepare($Query) or die('Unable to Query');
            $q->execute(array(
                ':id' => $this->id,
                ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $data = $q->fetch(PDO::FETCH_ASSOC);
                return $data;
            }else{
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    // get all news form tbl_news table
    public function newsIntex(){
        try {
            $Query = "SELECT * FROM tbl_news ORDER BY created_at DESC";
            $q = $this->conn->query($Query);
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $this->data[] = $row;
                }
                return $this->data;
            } else {
                return $rowCount;
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        return $this;
    }


    // add new news in tbl_news table
    public function storeNews() {
        if(!empty($this->title) && !empty($this->shotrDes) && !empty($this->details) && !empty($this->cat_id)
            && !empty($this->image) && !empty($this->tags)){
            try {
                $query = "INSERT INTO tbl_news (user_id,title,shortDes,details,cat_id,image,tags,created_at) VALUES(:uid,:t,:sd,:d,:cat_id,:img,:tags,:created_at)";
                $q = $this->conn->prepare($query);
                $q->execute(array(
                    ':uid' => $this->id,
                    ':t' => $this->title,
                    ':sd' => $this->shotrDes,
                    ':d' => $this->details,
                    ':cat_id' => $this->cat_id,
                    ':img' => $this->image,
                    ':tags' => $this->tags,
                    ':created_at' => date("Y-m-d G:i:s"),
                    ));
                $rows = $q->rowCount();
                if($rows>0){
                    $_SESSION['msg'] = '<font color="green">' . "Successfully News Published" . '</font>';
                    header('Location: index.php');
                } else {
                    $_SESSION['msg'] = '<font color="red">' . "Error while News Published" . '</font>';
                    header('Location: create.php');
                }
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        }else{
            header('Location: create.php');
        }
        return $this;
        
    }

    // check category
    public function checkCategory(){
        try {
            $chkQuery = "SELECT * FROM tbl_category WHERE title = :t ";
            $q = $this->conn->prepare($chkQuery) or die("Unable to Query");
            $q->execute(array(
                ':t' => $this->cat_name,
                ));
            $rowCount = $q->rowCount();
            return $rowCount;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }

    // store category into tbl_category table
    public function storeCategory() {
        if(!empty($this->cat_name)){
            try {
                $query = "INSERT INTO tbl_category (user_id,title,created_at) VALUES(:uid,:t,:created_at)";
                $q = $this->conn->prepare($query);
                $q->execute(array(
                    ':uid' => $this->id,
                    't' => $this->cat_name,
                    ':created_at' => date("Y-m-d G:i:s"),
                    ));
                $row = $q->rowCount();
                if ($row > 0) {
                    $_SESSION['catMsg'] = '<font color="green">' . "Successfully Category Added" . '</font>';
                    header('Location: createCategory.php');
                } else {
                    $_SESSION['catMsg'] = '<font color="red">' . "Error while Category Added" . '</font>';
                    header('Location: createCategory.php');
                }
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        }else{
            header('Location: createCategory.php');
        }
        return $this;
    }

    // get all category list

    public function categoryList() {
        try {
            $Query = "SELECT * FROM tbl_category ORDER BY title ASC";
            $q = $this->conn->query($Query);
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $this->data[] = $row;
                }
                return $this->data;
            } else {
                return $rowCount;
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        return $this;
    }



}
