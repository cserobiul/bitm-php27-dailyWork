<?php

namespace Apps\Users;

use PDO;
use PDOException;

class Users {
    public $id = '';
    public $full_name = '';
    public $username = '';
    public $email = '';
    public $uType = '';
    public $pass = '';
    public $conn = '';

    public function __construct() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=php27', 'root', '');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    // assgin value
    public function assign($data) {
        if (!empty($data['full_name'])) {
            $this->full_name = $data['full_name'];
        }

        if (!empty($data['username'])) {
            $this->username = $data['username'];
        }

        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }

        if (!empty($data['password'])) {
            $this->pass = $data['password'];
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        
        if (!empty($data['uType'])) {
            $this->uType = $data['uType'];
        } else {
            $_SESSION['pass'] = '<font color="red">' . "User Type Required" . '</font>';
        }

       
        if (!empty($data['tc'])) {
            $this->tc = $data['tc'];
        } else {
            $this->tc = 'n';
            $_SESSION['tc'] = '<font color="red">' . "Please Check Terms & Condition" . '</font>';
        }

        return $this;
    }

    // get username by u_id
     public function getUsername(){
        try {
            $Query = "SELECT * FROM tbl_users WHERE unique_id = :id";
            $q = $this->conn->prepare($Query);
            $q->execute(array(
                ':id' => $this->id,
                ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $data = $q->fetch(PDO::FETCH_ASSOC);
                return $data;
            }else{
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    // data store

    public function store(){
        try {
            $query = "INSERT INTO tbl_users(unique_id,fullName,username, email, password,is_admin, is_active, created_at) VALUES(:uId, :fname, :u, :e, :p, :is_admin, :is_active,:created_at)";
            $q = $this->conn->prepare($query);
            $q->execute(array(
                ':uId' => uniqid(),
                ':fname' => $this->full_name,
                ':u' => $this->username,
                ':e' => $this->email,
                ':p' => $this->pass,
                ':is_admin' => '0',
                ':is_active' => '1',
                ':created_at' => date("Y-m-d G:i:s"),
            ));
            $row = $q->rowCount();
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    // check username
     public function checkUsername(){
        try {
            $chkQuery = "SELECT * FROM tbl_users WHERE username = :u ";
            $q = $this->conn->prepare($chkQuery);
            $q->execute(array(
                ':u' => $this->username,
            ));
            $rowCount = $q->rowCount();
            return $rowCount;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }

    //check email

    public function checkEmail(){
        try {
            $chkQuery = "SELECT * FROM tbl_users WHERE email = :e ";
            $q = $this->conn->prepare($chkQuery);
            $q->execute(array(
                ':e' => $this->email,
            ));
            $rowCount = $q->rowCount();
            return $rowCount;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }
    
    // user login
    public function login()
    {
        try {
            $loginQuery = "SELECT * FROM tbl_users WHERE (username = :u || email = :u) && password = :p && is_active = :ac ";
            $q = $this->conn->prepare($loginQuery);
            $q->execute(array(
                ':u' => $this->username,
                ':p' => $this->pass,
                ':ac' => '1',
            ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $row = $q->fetch(PDO::FETCH_ASSOC);
                return $row;
            } else {
                $_SESSION['logMsg'] = "Invalid Information or <br> Your account is not Active";
                header("location:login.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }
    
   

}
