<?php
include_once '../../vendor/autoload.php';
$obj = new Apps\News\News();
$data = $obj->categoryList();

if (!isset($_SESSION['loginUser']) && empty($_SESSION['loginUser'])) {
    $_SESSION['logMsg'] = 'Login  First';
    header("location:../Users/login.php");
}
?>
<html>
<head>
    <title>Add News</title>
</head>
<body>
    <a href="../Users/index.php">Dashboard</a> ||
    <a href="index.php">Blog</a> ||
    <?php if($_SESSION['is_admin'] == 1){ ?>
        <a href="../News/createCategory.php">Add Category</a> ||
        <?php } ?>
        <a href="../Users/logout.php">Logout</a>
        <form action="store.php" method="POST" enctype="multipart/form-data">

            <table border="0" cellpadding="0">
                <tr>
                    <td colspan="2"><h3>Add New News</h3>
                        <?php if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                            echo $_SESSION['msg'];
                            unset($_SESSION['msg']);
                        }
                        ?>
                    </td>

                </tr>
                <tr>
                    <th>News Title</th>
                    <td><input name="newsTitle" type="text" size="100" value="<?php if(isset($_SESSION['titleValue']) && !empty($_SESSION['titleValue'])){
                        echo $_SESSION['titleValue'];
                        unset($_SESSION['titleValue']);
                    } ?>"> 
                </td>

            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['titleErrMsg']) && !empty($_SESSION['titleErrMsg'])) {
                        echo $_SESSION['titleErrMsg'];
                        unset($_SESSION['titleErrMsg']);
                    } ?>
                </td> 
            </tr>
            <tr>
                <th>Short Description</th>
                <td><textarea name="shotrDes" cols="90" rows="5"><?php if(isset($_SESSION['shotrDesValue']) && !empty($_SESSION['shotrDesValue'])){
                    echo $_SESSION['shotrDesValue'];
                    unset($_SESSION['shotrDesValue']);
                } ?></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['shotrDesErrMsg']) && !empty($_SESSION['shotrDesErrMsg'])) {
                        echo $_SESSION['shotrDesErrMsg'];
                        unset($_SESSION['shotrDesErrMsg']);
                    } ?>
                </td>
            </tr>   
            <tr>
                <th>Details</th>
                <td><textarea name="details" cols="90" rows="10"><?php if(isset($_SESSION['detailsValue']) && !empty($_SESSION['detailsValue'])){
                    echo $_SESSION['detailsValue'];
                    unset($_SESSION['detailsValue']);
                } ?></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['detailsErrMsg']) && !empty($_SESSION['detailsErrMsg'])) {
                        echo $_SESSION['detailsErrMsg'];
                        unset($_SESSION['detailsErrMsg']);
                    } ?>
                </td>
            </tr> 
            <tr>
                <th>Category</th>
                <td>
                    <?php if (!empty($data)) { ?>
                        <select name="cat_id">
                            <option value="0">Select One</option>
                            <?php
                            foreach ($data as $value) { ?>
                                <option value="<?php echo $value['id']  ?>"><?php echo $value['title']  ?></option>       
                                <?php }
                                ?>


                            </select>
                            <?php
                        } else {
                            echo 'NO Category';
                        }
                        ?>

                        

                    </td>
                </tr> 
                <tr>
                    <td></td>
                    <td>
                        <?php if (isset($_SESSION['cat_idErrMsg']) && !empty($_SESSION['cat_idErrMsg'])) {
                            echo $_SESSION['cat_idErrMsg'];
                            unset($_SESSION['cat_idErrMsg']);
                        } ?>
                    </td>
                </tr> 
                <tr>
                    <th>News Tag</th>
                    <td><input name="newsTag" type="text" size="80" value="<?php if(isset($_SESSION['newsTagValue']) && !empty($_SESSION['newsTagValue'])){
                        echo $_SESSION['newsTagValue'];
                        unset($_SESSION['newsTagValue']);
                    } ?>"> </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <?php if (isset($_SESSION['newsTagErrMsg']) && !empty($_SESSION['newsTagErrMsg'])) {
                            echo $_SESSION['newsTagErrMsg'];
                            unset($_SESSION['newsTagErrMsg']);
                        } ?>
                    </td>
                </tr>
                <tr>
                    <th>News Image</th>
                    <td><input name="photo" type="file"> </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <?php if (isset($_SESSION['photoErrMsg']) && !empty($_SESSION['photoErrMsg'])) {
                            echo $_SESSION['photoErrMsg'];
                            unset($_SESSION['photoErrMsg']);
                        } ?>                   
                    </td>
                </tr>

                <tr>
                    <th><input name="" type="reset" value="Clear "></th>
                    <td><input name="" type="submit" value="Published"> </td>
                </tr>
            </table>

        </form>  
    </body>
    </html>

