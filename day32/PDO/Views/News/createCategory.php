<?php
session_start();
if (!isset($_SESSION['loginUser']) && empty($_SESSION['loginUser'])) {
    $_SESSION['logMsg'] = 'Login  First';
    header("location:../Users/login.php");
}

if($_SESSION['is_admin'] == 0){ 
    $_SESSION['logMsg'] = 'Login  First';
    header("location:../Users/login.php");
}
?>
<html>
    <head>
        <title>Add Category</title>
    </head>
    <body>
        <a href="../Users/index.php">Dashboard</a> ||
        <a href="index.php">Blog</a> ||
        <a href="create.php">Add News</a> ||
        <a href="../Users/logout.php">Logout</a>
        <form action="storeCategory.php" method="POST" enctype="multipart/form-data">

            <table border="0" cellpadding="0">
                <tr>
                    <td colspan="2"><h3>Add New Category</h3></td>

                </tr>
                
                <tr>
                    <th>Category Name</th>
                    <td><input name="cat_name" type="text" size="40"> </td>
                </tr>
                <tr>
                    <td><?php //echo 'ID'. $_SESSION['uID'] ?></td>
                    <td><?php
                        if(isset($_SESSION['catMsg']) && !empty($_SESSION['catMsg'])){
                            echo $_SESSION['catMsg'];
                            unset($_SESSION['catMsg']);
                        }
                        ?></td>
                </tr>

                <tr>
                    <th><input name="" type="reset" value="Clear "></th>
                    <td><input name="" type="submit" value="Add Category"> </td>
                </tr>
            </table>

        </form>  
    </body>
</html>

