<title>Latest News </title>
<style type="text/css">
	a{ text-decoration: none; }
	a:hover{ text-decoration: underline; }
</style>
<?php

include_once '../../vendor/autoload.php';
$dataObj = new Apps\News\News();
$userObj = new Apps\Users\Users();

if (!isset($_SESSION['loginUser']) && empty($_SESSION['loginUser'])) {
	$_SESSION['logMsg'] = 'Login  First';
	header("location:../Users/login.php");
}
?>
<?php if($_SESSION['is_admin'] == 1){ ?>
<a href="../News/createCategory.php">Add Category</a> ||
<?php } ?>
<a href="../News/create.php">Add News</a> ||
<a href="../Users/logout.php">Logout</a>

<br>
<?php 

if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
	echo $_SESSION['msg'];
	unset($_SESSION['msg']);
}

$data = $dataObj->newsIntex();
//echo '<pre>';
//print_r($data);

?>

	<caption><h1>All Latest News</h1></caption>
<?php if(!empty($data)){ 
	foreach ($data as $key) {?>
		<div style="height:auto;border: 1px solid black; padding:10px; margin-bottom: 5px;">
		<div style="float: left; padding: 15px; ">
			<img width="225" height="150" src="<?php echo '../../img/'.$key['image'] ?>">
			
		</div>
		<div>
			<p><a href="show.php?id=<?php echo $key['id']; ?>" style="text-decoration: none;"> <?php echo $key['title'] ?></a> </p>
			<p><?php echo $key['shortDes'] ?>
				<a href="show.php?id=<?php echo $key['id']; ?>">Read More</a></p>
				<p>Author:
			<?php 
			$_REQUEST['id']=$key['user_id'];
			$getUsername = $userObj->assign($_REQUEST)->getUsername();
			if(!empty($getUsername)){
				$uId=$getUsername['unique_id'];
				echo '<a href="../Users/profile.php?id='.$uId.'">'.ucwords($getUsername['fullName']).'</a> ' ;
				
				
				echo ' <i>at</i> '.date_format(date_create($key['created_at']),"h:i a");			
				echo ' <i>on</i> '.date_format(date_create($key['created_at']),"d-M-Y ");			
			}else{
				echo 'N/A';
			}
			?>		
				</p>
		</div>
			
		</div>
	<?php 	}
	}else{
		echo 'No News';
		} ?>
