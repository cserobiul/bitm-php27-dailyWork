<title>Single News </title>
<style type="text/css">
	a{ text-decoration: none; }
	a:hover{ text-decoration: underline; }
</style>
<?php
include_once '../../vendor/autoload.php';
$showObj = new Apps\News\News();
$userObj = new Apps\Users\Users();
$data = $showObj->assign($_REQUEST)->show();

if (!isset($_SESSION['loginUser']) && empty($_SESSION['loginUser'])) {
	$_SESSION['logMsg'] = 'Login  First';
	header("location:../Users/login.php");
}

if(!empty($data)){ ?>
	<a href="index.php">Blog</a>
	<table border="0" cellpadding="0" >
		<thead>
			<tr align='center'>
				<td><h2><a href="show.php?id=<?php echo $data['id']; ?>" style="text-decoration: none;"> <?php echo $data['title'] ?></a> </h2></td>
			</tr>
			<tr align='center'>
				<td><img width="500" height="300" src="<?php echo '../../img/'.$data['image'] ?>"></td>
			</tr>
			<tr>
				<td> 			
					<?php echo $data['details'] ?>					
				</td>
			</tr>
		</thead>
	</table>
	<div class="tag">
		<h4>Tags:
			<?php 
			$tags = explode(',', $data['tags']) ;

			foreach ($tags as $tag => $value) {
				if($tag!=0){
					echo '<a href="tag.php?newsTag='.$value.'">,'.ucwords($value).'</a>';
				}else{
					echo '<a href="tag.php?newsTag='.$value.'">'.ucwords($value).'</a>';
				}	
			}
			?>
		</h4>
	</div>
	<div class="category">
		<h4>Category:
			<?php 
			$_REQUEST['cat_id']=$data['cat_id'];
			$catName = $showObj->assign($_REQUEST)->categoryName();
			if(!empty($catName)){
				$catId=$catName['id'];
				echo '<a href="category.php?cat_id='.$catId.'">'.$catName['title'].'</a>';				
			}else{
				echo 'N/A';
			}
			?>
		</h4>
	</div>

	<div class="author">
		<h4>Post By:
			<?php 
			$_REQUEST['id']=$data['user_id'];
			$getUsername = $userObj->assign($_REQUEST)->getUsername();
			if(!empty($getUsername)){
				$uId=$getUsername['unique_id'];
				echo '<a href="../Users/profile.php?id='.$uId.'">'.ucwords($getUsername['fullName']).'</a> ' ;
				
				
				echo ' <i>at</i> '.date_format(date_create($data['created_at']),"h:i a");			
				echo ' <i>on</i> '.date_format(date_create($data['created_at']),"d-M-Y ");			
			}else{
				echo 'N/A';
			}
			?>
		</h4>
	</div>

	<h2>Related News</h2>
	<?php }else{
		$_SESSION['errMsg'] = "You are Unauthorized user";
		header('location:../Users/404.php');
	} ?>


	<?php 
	$_REQUEST['cat_id']=$data['cat_id'];
	$relatedData = $showObj->assign($_REQUEST)->relatedNews();
	if(!empty($relatedData)){ 
		foreach ($relatedData as $item) { ?>
			<div style="float:left; padding-right: 10px">
				<p align="center">
					<img width="150" height="120" src="<?php echo '../../img/'.$item['image'] ?>">
				</p>
				<p align="center">
					<a href="show.php?id=<?php echo $item['id']; ?>" style="text-decoration: none;"> <?php echo $item['title'] ?></a>
				</p>
			</div>

			<?php } }else{
				echo 'No Related News';
			}?>