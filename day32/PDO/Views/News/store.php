<?php
include_once '../../vendor/autoload.php';
$newsobj = new Apps\News\News();

echo '<pre>';
$_REQUEST['id']=$_SESSION['uID'];

if(isset($_FILES['photo'])){

	$errors = array();

	if(empty($_REQUEST['newsTitle'])){
		$errors[] ='News Title Required...';
		$_SESSION['titleErrMsg'] = '<font color="red">News Title Required...</font>';
	}elseif(strlen(trim($_REQUEST['newsTitle']))<=20 || strlen(trim($_REQUEST['newsTitle']))>=70){
		$errors[] ='News Title Must be 20 to 70 Chars...';
		$_SESSION['titleErrMsg'] = '<font color="red">News Title Must be 20 to 70 Chars...</font>';
		$_SESSION['titleValue'] = $_REQUEST['newsTitle'];
	}

	if(empty($_REQUEST['shotrDes'])){
		$errors[] ='Short Description Required...';
		$_SESSION['shotrDesErrMsg'] = '<font color="red">Short Description Required...</font>';
	}elseif(strlen(trim($_REQUEST['shotrDes']))<=100 || strlen(trim($_REQUEST['shotrDes']))>=500){
		$errors[] ='Short Description Must be 100 to 500 Chars...';
		$_SESSION['shotrDesErrMsg'] = '<font color="red">Short Description Must be 100 to 500 Chars...</font>';
		$_SESSION['shotrDesValue'] = $_REQUEST['shotrDes'];
	}

	if(empty($_REQUEST['details'])){
		$errors[] ='News Details Required...';
		$_SESSION['detailsErrMsg'] = '<font color="red">News Details Required...</font>';
	}elseif(strlen(trim($_REQUEST['details']))<=1000){
		$errors[] ='News Details Minimum 1000 Chars...';
		$_SESSION['detailsErrMsg'] = '<font color="red">News Details Minimum 1000 Chars......</font>';
		$_SESSION['detailsValue'] = $_REQUEST['details'];
	}

	if(empty($_REQUEST['cat_id'])){
		$errors[] ='Category Required...';
		$_SESSION['cat_idErrMsg'] = '<font color="red">Category Required...';
	}

	if(empty($_REQUEST['newsTag'])){
		$errors[] ='Tag Required...';
		$_SESSION['newsTagErrMsg'] = '<font color="red">Tag Required...</font>';
	}else{
		$_SESSION['newsTagValue'] = $_REQUEST['newsTag'];
	}

	$filename = time().'_'.uniqid();
	$filetype = $_FILES['photo']['type'];
	$filetemp = $_FILES['photo']['tmp_name'];
	$filesize = $_FILES['photo']['size'];
	$fileext = strtolower(end(explode('.', $_FILES['photo']['name'])));

	$formats = array("jpeg","jpg","png");

	if(in_array($fileext, $formats) === false){
		$errors[]="Photo Extension not allowed";
		$_SESSION['photoErrMsg'] = '<font color="red">Photo Extension not allowed...</font>';		
	}

	if($filesize>2097152){
		$errors[]="File size must be excately 2 MB";
		$_SESSION['photoErrMsg'] = '<font color="red">File size must be excately 2 MB...</font>';
	}

	if(empty($errors) == true){
		move_uploaded_file($filetemp, "../../img/".$filename.'.'.$fileext);
		$_REQUEST['img']=$filename.'.'.$fileext;
	}else{
		header('Location: create.php');
		foreach ($errors as $key) {
			echo '<p style="color:red;">'.$key.'</p>';
		}
		
	}
}


// print_r($_REQUEST);
// print_r($_FILES);

$data = $newsobj->assign($_REQUEST)->storeNews();


?>
