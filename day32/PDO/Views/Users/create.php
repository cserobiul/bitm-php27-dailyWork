<?php
include_once '../../vendor/autoload.php';
$dataObj = new Apps\Users\Users();


if (isset($_SESSION['loginUser']) && !empty($_SESSION['loginUser'])) {
    if($_SESSION['is_admin']==0){
        $_SESSION['errMsg'] = 'Unauthorized User';
        header("location:404.php");
    }
    
}


?>
<html>
<head>
    <title>Add User</title>
</head>
<body>
    <form action="store.php" method="POST" enctype="multipart/form-data">
        <a href="index.php">Home </a>
        <table border="0" cellpadding="0">
            <tr>
                <td colspan="2"><h3>Add New User</h3></td>

            </tr>
            <tr>
                <th>Full Name</th>
                <td><input name="full_name" type="text" size="40" value="<?php if (isset($_SESSION['full_nameValue']) && !empty($_SESSION['full_nameValue'])) {
                    echo $_SESSION['full_nameValue'];
                    unset($_SESSION['full_nameValue']);
                } ?>"> </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['full_nameErrMsg']) && !empty($_SESSION['full_nameErrMsg'])) {
                        echo $_SESSION['full_nameErrMsg'];
                        unset($_SESSION['full_nameErrMsg']);
                    } ?>
                </td> 
            </tr>
            <tr>
                <th>Username</th>
                <td><input name="username" type="text" size="40" value="<?php if (isset($_SESSION['usernameValue']) && !empty($_SESSION['usernameValue'])) {
                    echo $_SESSION['usernameValue'];
                    unset($_SESSION['usernameValue']);
                } ?>">   </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['usernameErrMsg']) && !empty($_SESSION['usernameErrMsg'])) {
                        echo $_SESSION['usernameErrMsg'];
                        unset($_SESSION['usernameErrMsg']);
                    } ?>
                </td> 
            </tr>
            <tr>
                <th>Email</th>
                <td><input name="email" type="email" size="40" value="<?php if (isset($_SESSION['emailValue']) && !empty($_SESSION['emailValue'])) {
                    echo $_SESSION['emailValue'];
                    unset($_SESSION['emailValue']);
                } ?>">    </td>
            </tr> 
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['emailErrMsg']) && !empty($_SESSION['emailErrMsg'])) {
                        echo $_SESSION['emailErrMsg'];
                        unset($_SESSION['emailErrMsg']);
                    } ?>
                </td> 
            </tr>
            <tr>
                <th>Password</th>
                <td><input name="password" type="password" size="40"> </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['passwordErrMsg']) && !empty($_SESSION['passwordErrMsg'])) {
                        echo $_SESSION['passwordErrMsg'];
                        unset($_SESSION['passwordErrMsg']);
                    } ?>
                </td> 
            </tr>

            <tr>
                <th>Repeated Password</th>
                <td><input name="rpassword" type="password" size="40"> </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['rpasswordErrMsg']) && !empty($_SESSION['rpasswordErrMsg'])) {
                        echo $_SESSION['rpasswordErrMsg'];
                        unset($_SESSION['rpasswordErrMsg']);
                    } ?>
                </td> 
            </tr>

            <tr>
                <th>Terms & Condition</th>
                <td><input name="tc" type="checkbox" value="y"> </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <?php if (isset($_SESSION['tcErrMsg']) && !empty($_SESSION['tcErrMsg'])) {
                        echo $_SESSION['tcErrMsg'];
                        unset($_SESSION['tcErrMsg']);
                    } ?>
                </td> 
            </tr>

            <tr>
                <th><input name="" type="reset" value="Clear "></th>
                <td><input name="" type="submit" value="Submit"> </td>
            </tr>
        </table>

    </form>  
</body>
</html>

