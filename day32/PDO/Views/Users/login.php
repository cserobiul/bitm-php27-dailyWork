<?php 
include_once('../../vendor/autoload.php');
$loginObj = new \Apps\Users\Users;

if (isset($_SESSION['loginUser']) && !empty($_SESSION['loginUser'])) {
    header("location:index.php");
}
?>
<html>
    <head>
        <title>Login</title>
    </head>
    <body>
        <form action="login_process.php" method="POST">
            
            <table border="0" cellpadding="0">
                <tr>
                    <td colspan="2"><h3>Login Page</h3></td>
                    
                </tr>
                <tr>
                    <th>Username or Email</th>
                    <td><input name="username" type="text" size="40">   </td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td><input name="password" type="password" size="40"> </td>
                </tr>
                <?php
            if (isset($_SESSION['logMsg']) && !empty($_SESSION['logMsg'])) { ?>
                <tr>
                    <td></td>
                    <td >
                        <?php
                            echo '<strong><font color="#b22222">' . $_SESSION['logMsg'] . '</font></strong>';
                            unset($_SESSION['logMsg']);
                            ?>
                    </td>
                </tr>
            <?php } ?>
            

            <?php
            // when  data successfully submitted then working
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) { ?>
                <tr>
                    <td></td>
                    <td >
                        <?php
                            echo '<strong><font color="green">' . $_SESSION['msg'] . '</font></strong>';
                            unset($_SESSION['msg']);
                            ?>
                    </td>
                </tr>
            <?php } ?>
                    
                
                <tr>
                    <th><input name="" type="reset" value="Clear "></th>
                    <td><input name="" type="submit" value="Login">
                    <a href="create.php">Sign Up</a></td>
                </tr>
            </table>
            
        </form>  
    </body>
</html>

