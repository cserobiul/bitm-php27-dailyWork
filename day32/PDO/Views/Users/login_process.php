<?php
include_once('../../vendor/autoload.php');
$loginObj = new \Apps\Users\Users;

if(!empty($_POST['username']) && !empty($_POST['password'])){
    $logData = $loginObj->assign($_POST)->login();

//    echo '<pre>';
//    print_r($logData);
    $_SESSION['loginUser'] = $logData['username'];
    $_SESSION['uID'] = $logData['unique_id'];
    $_SESSION['is_admin'] = $logData['is_admin'];
    header("location:index.php");


}else{
    $_SESSION['logMsg'] = "* Both filed are required";
    header("location:login.php");
}