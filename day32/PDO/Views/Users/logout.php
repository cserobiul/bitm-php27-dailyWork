<?php
session_start();
if(isset($_SESSION['loginUser']) && !empty($_SESSION['loginUser'])){
    unset($_SESSION['loginUser']);
    unset($_SESSION['is_admin']);
    unset($_SESSION['u_id']);
    $_SESSION['logMsg']= '<font color="green">Successfully Logout</font>';
    header("location:login.php");

}else{
    $_SESSION["errMsg"]= "Your are Unauthorized user";
    header("location:404.php");
}