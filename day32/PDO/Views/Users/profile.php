<title>Profile Info </title>
<style type="text/css">
	a{ text-decoration: none; }
	a:hover{ text-decoration: underline; }
</style>
<?php 
include_once '../../vendor/autoload.php';
$profileObj = new Apps\Users\Users();

$user = $profileObj->assign($_REQUEST)->getUsername();
?>
<a href="../News/index.php">Blog</a>
<?php 
if(!empty($user)){ ?>
	<table border="1" cellpadding="5">
		<caption>Profile Info</caption>
			<tr>
				<th>Full Name: </th>
				<td><?php echo ucwords($user['fullName']) ?></td>
			</tr>
			<tr>
				<th>Username: </th>
				<td><?php echo $user['username'] ?></td>
			</tr>
			<tr>
				<th>Email: </th>
				<td><?php echo $user['email'] ?></td>
			</tr>
			<tr>
				<th>Join at: </th>
				<td><?php		
				echo date_format(date_create($user['created_at']),"d-M-Y ");	?></td>
			</tr>
		</table>		

<?php }else{
	$_SESSION['errMsg'] = 'Unauthorized User';
    header("location:404.php");
}
 

?>
