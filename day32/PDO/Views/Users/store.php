<?php
include_once '../../vendor/autoload.php';
$storeObj = new Apps\Users\Users();

echo '<pre>';
echo '</pre>';


if(!empty($_REQUEST['tc'])){

	$chkUser = $storeObj->assign($_REQUEST)->checkUsername();
	$chkEmail = $storeObj->assign($_REQUEST)->checkEmail();

	$errors = array();

	// full_name 
	if(empty($_REQUEST['full_name'])){
		$errors[] ='Full Name Required...';
		$_SESSION['full_nameErrMsg'] = '<font color="red">Full Name Required...</font>';
	}elseif(strlen(trim($_REQUEST['full_name'])) <6 || strlen(trim($_REQUEST['full_name']))>50){
		$errors[] ='Full Name Must be 6 to 50 Chars...';
		$_SESSION['full_nameErrMsg'] = '<font color="red">Full Name Must be 6 to 50 Chars...</font>';
		$_SESSION['full_nameValue'] = $_REQUEST['full_name'];
	}

	// username
	
		if(empty($_REQUEST['username'])){
			$errors[] ='Username Required...';
			$_SESSION['usernameErrMsg'] = '<font color="red">Username Required...</font>';
		}elseif($chkUser > 0){
			$errors[] ='Username Already Exist...';
			$_SESSION['usernameErrMsg'] = '<font color="red">Username Already Exist</font>';
			unset($_REQUEST['username']);
		}elseif(strlen(trim($_REQUEST['username']))<3 || strlen(trim($_REQUEST['username']))>20){
			$errors[] ='Username Must be 3 to 20 Chars...';
			$_SESSION['usernameErrMsg'] = '<font color="red">Username Must be 3 to 20 Chars...</font>';
			$_SESSION['usernameValue'] = $_REQUEST['username'];
		}
	

	// email
	
		if(empty($_REQUEST['email'])){
			$errors[] ='Email Required...';
			$_SESSION['emailErrMsg'] = '<font color="red">Email Required...</font>';
		}elseif($chkEmail > 0){
			$errors[] ='Email Already Exist...';
			$_SESSION['emailErrMsg'] = '<font color="red">Email Already Exist...</font>';
			unset($_REQUEST['email']);
		}else{
			$_SESSION['emailValue'] = $_REQUEST['email'];
		}

	// password 

	if (!empty($_REQUEST['password']) && !empty($_REQUEST['rpassword'])) {
		if($_REQUEST['password'] != $_REQUEST['rpassword']){
			$errors[] ='Password Did not match...';
			$_SESSION['passwordErrMsg'] = '<font color="red">' . "Password Did not match" . '</font>'; 
		}else{
			if(strlen(trim($_REQUEST['password']))<6 || strlen(trim($_REQUEST['password']))>10){
				$errors[] ='Password Must be 6 to 10 Chars...';
				$_SESSION['passwordErrMsg'] = '<font color="red">Password Must be 6 to 10 Chars......</font>';
			}
			if(strlen(trim($_REQUEST['rpassword']))<6 || strlen(trim($_REQUEST['rpassword']))>10){
				$errors[] ='Repeted Password Must be 6 to 10 Chars...';
				$_SESSION['rpasswordErrMsg'] = '<font color="red">Repeted Password Must be 6 to 10 Chars......</font>';
			}			
		}    
	} else {
		$errors[] ='Password Required...';
		$_SESSION['passwordErrMsg'] = '<font color="red">' . "Password Required" . '</font>';
		$_SESSION['rpasswordErrMsg'] = '<font color="red">' . "Repeted Password Required" . '</font>';
	}

	if(empty($errors) == true){
		$data = $storeObj->assign($_REQUEST)->store();
		if($data == 1){
            $_SESSION['msg'] = "Successfully Data Submitted <br>Please Login First";
            unset($_SESSION['full_nameValue']);
            unset($_SESSION['usernameValue']);
            unset($_SESSION['emailValue']);
            header("location:login.php");
        }else{
            $_SESSION['msg'] = "Error While Data Submitted";
            header("location:login.php");
        }
	}else{
		header('Location: create.php');
		foreach ($errors as $key) {
			echo '<p style="color:red;">'.$key.'</p>';
		}
	}
}else{
	$_SESSION['tcErrMsg'] = '<font color="red">Please Check  Terms & Condition First</font>';
	header('Location:create.php');
}






?>
