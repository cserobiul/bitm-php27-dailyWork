-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2016 at 06:52 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `user_id`, `title`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, '25698745422', 'Bangladesh', '2016-08-04 13:04:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '25698745422', 'Sports', '2016-08-04 13:05:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '25698745422', 'Internet', '2016-08-04 13:05:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '25698745422', 'Technology', '2016-08-04 13:06:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '25698745422', 'Mobile', '2016-08-04 13:07:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '25698745422', 'Computer', '2016-08-04 13:14:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '57906272d0888', 'Newspaper', '2016-08-05 04:39:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '57906272d0888', 'International', '2016-08-05 06:41:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '57906272d0888', 'Programming', '2016-08-05 06:48:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '57906272d0888', 'Life-Style', '2016-08-05 18:51:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '57906272d0888', 'Food', '2016-08-05 18:51:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '57906272d0888', 'Hacking', '2016-08-06 05:54:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
