-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2016 at 06:53 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `shortDes` text NOT NULL,
  `details` text NOT NULL,
  `cat_id` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `user_id`, `title`, `shortDes`, `details`, `cat_id`, `image`, `tags`, `created_at`, `modified_at`, `deleted_at`, `is_deleted`) VALUES
(6, '57906272d0888', 'Why use PDO?', 'Instead of using mysql_num_rows to get the number of returned rows you can get a PDOStatement and do rowCount(); ', 'Instead of using mysql_num_rows to get the number of returned rows you can get a PDOStatement and do rowCount(); Instead of using mysql_num_rows to get the number of returned rows you can get a PDOStatement and do rowCount(); ', '9', '1470372612_57a41b0413ff1.png', 'pdo,programming,database', '2016-08-05 06:50:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(7, '57906272d0888', 'Luis Suarez ', 'Luis Suarez and Lionel Messi lead Barcelona past LeicesterLuis Suarez and Lionel Messi lead Barcelona past Leicester', 'Luis Suarez and Lionel Messi lead Barcelona past LeicesterLuis Suarez and Lionel Messi lead Barcelona past LeicesterLuis Suarez and Lionel Messi lead Barcelona past LeicesterLuis Suarez and Lionel Messi lead Barcelona past LeicesterLuis Suarez and Lionel Messi lead Barcelona past Leicester', '2', '1470374670_57a4230eab3c0.jpg', 'messi, football', '2016-08-05 07:24:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(8, '57a677689eaf6', 'Epic burger battles', 'Burgers are in! But when were they ''out'' though? Nevertheless, given that Dhaka today has a myriad range of some fantastic burgers and burger places than ever before, the ''burger fever'' is indeed at its peak.', 'Burgers are in! But when were they ''out'' though? Nevertheless, given that Dhaka today has a myriad range of some fantastic burgers and burger places than ever before, the ''burger fever'' is indeed at its peak.Burgers are in! But when were they ''out'' though? Nevertheless, given that Dhaka today has a myriad range of some fantastic burgers and burger places than ever before, the ''burger fever'' is indeed at its peak.Burgers are in! But when were they ''out'' though? Nevertheless, given that Dhaka today has a myriad range of some fantastic burgers and burger places than ever before, the ''burger fever'' is indeed at its peak.', '10', '1470415930_57a4c43a4e642.png', 'food, burger', '2016-08-05 18:52:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(9, '57906272d0888', 'Top Three Programming ', 'If youâ€™re planning on tinkering around with electronics, whether itâ€™s on a Raspberry Pi, Arduino, or anything else, youâ€™ll need to know at least a little bit of programming. Hackster.io sent out a survey to figure out which three languages people thought were the best to learn.', 'If youâ€™re planning on tinkering around with electronics, whether itâ€™s on a Raspberry Pi, Arduino, or anything else, youâ€™ll need to know at least a little bit of programming. Hackster.io sent out a survey to figure out which three languages people thought were the best to learn.If youâ€™re planning on tinkering around with electronics, whether itâ€™s on a Raspberry Pi, Arduino, or anything else, youâ€™ll need to know at least a little bit of programming. Hackster.io sent out a survey to figure out which three languages people thought were the best to learn.If youâ€™re planning on tinkering around with electronics, whether itâ€™s on a Raspberry Pi, Arduino, or anything else, youâ€™ll need to know at least a little bit of programming. Hackster.io sent out a survey to figure out which three languages people thought were the best to learn.', '9', '1470417318_57a4c9a650a39.jpg', 'programming, electronics', '2016-08-05 19:15:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(10, '57906272d0888', 'VisualStudio (version1.4)', 'During July, we slowed down feature work in favor of reducing our bug backlog and removing engineering debt. However, we were still able to add some improvements.', 'During July, we slowed down feature work in favor of reducing our bug backlog and removing engineering debt. However, we were still able to add some improvements.\n', '9', '1470418619_57a4cebbe594f.jpg', 'visual studio,programming ,software', '2016-08-05 19:36:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(11, '57a67863e767b', 'PHP Composer :: Dependency management', '\r\nYou can place the Composer PHAR anywhere you wish. If you put it in a directory that is part of your PATH, you can access it globally. On unixy systems you can even make it executable and invoke it without directly using the php interpreter.', 'Dependency management#\n\nComposer is not a package manager in the same sense as Yum or Apt are. Yes, it deals with "packages" or libraries, but it manages them on a per-project basis, installing them in a directory (e.g. vendor) inside your project. By default it does not install anything globally. Thus, it is a dependency manager. It does however support a "global" project for convenience via the global command.\n\nThis idea is not new and Composer is strongly inspired by node''s npm and ruby''s bundler.\n\nSuppose:\n\n    You have a project that depends on a number of libraries.\n    Some of those libraries depend on other libraries.\n\nComposer:\n\n    Enables you to declare the libraries you depend on.\n    Finds out which versions of which packages can and need to be installed, and installs them (meaning it downloads them into your project).\n\nSee the Basic usage chapter for more details on declaring dependencies.\nSystem Requirements#\n\nComposer requires PHP 5.3.2+ to run. A few sensitive php settings and compile flags are also required, but when using the installer you will be warned about any incompatibilities.\n\nTo install packages from sources instead of simple zip archives, you will need git, svn, fossil or hg depending on how the package is version-controlled.\n\nComposer is multi-platform and we strive to make it run equally well on Windows, Linux and OSX.\nInstallation - Linux / Unix / OSX#\nDownloading the Composer Executable#\n', '9', '1470530523_57a683db998b0.png', 'Programming,php,composer', '2016-08-07 02:42:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(12, '57906272d0888', 'HMMâ€™s Restructuring Pushed to Late July', 'The creditor-led restructuring scheme of the South Koreaâ€™s cash-strapped shipping firm Hyundai Merchant Marine (HMM) has been pushed to late July as the company struggles to find a global shipping alliance, according to Yonhap News Agency.', 'The creditor-led restructuring scheme of the South Koreaâ€™s cash-strapped shipping firm Hyundai Merchant Marine (HMM) has been pushed to late July as the company struggles to find a global shipping alliance, according to Yonhap News Agency.\r\n\r\nUnder the terms set by its creditors, led by the state-run Korea Development Bank (KDB), HMM needs to fulfill the restructuring prerequisites, which include a debt recast, reaching agreement on charter rate cuts and becoming a part of a global shipping alliance.\r\n\r\nLast week the company started discussions to join the worldâ€™s largest alliance 2M, which consist of the Danish shipping firm Maersk Line and Mediterranean Shipping Company (MSC).\r\n\r\nThe talks on the possibility of HMM joining the 2M vessel sharing agreement (2M VSA) after the companyâ€™s membership in the G6 alliance expires in 2017, are still in the early days, Maersk Lineâ€™s COO, SÃ¸ren Toft, told World Maritime News.\r\n\r\nâ€œWe are positively reviewing the proposition. The inclusion of Hyundai Merchant Marine in 2M would for example provide us with extended coverage and a stronger product in the Transpacific trade,â€ he added.\r\n\r\nThe announcement follows HMMâ€™s agreements with containership owners on 20 percent charter rate cuts and with owners of bulk carriers for 25 percent charter rate reductions, which are expected to come in force over the next three and a half years.\r\n\r\nFurthermore, HMM gained bond holdersâ€™ approval for its debt restructuring proposal, as the company faces USD 4.48 billion wall of debt.\r\n\r\nWorld Maritime News Staff', '4', '1470588520_57a7666884333.png', 'Restructuring ,Business', '2016-08-07 18:48:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
