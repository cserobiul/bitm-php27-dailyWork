-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2016 at 06:53 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(50) NOT NULL,
  `fullName` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `unique_id`, `fullName`, `username`, `email`, `password`, `is_admin`, `is_active`, `is_delete`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, '57906272d0888', 'arif khan joy', 'arif', 'arif25@gmail.com', '123', 1, 1, 0, '2016-08-04 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '57906202e0888', 'robiul islam', 'robi', 'amitumi@gmail.com', 'aaa', 0, 1, 0, '2016-08-04 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '57a676dbe180f', 'Toyabur rahaman', 'toabur', 'toabur454@gmail.com', '0987654', 0, 1, 0, '2016-08-07 01:46:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '57a677689eaf6', 'joy bissas', 'joy', 'joy82726@hotmail.com', 'joyjoy0', 0, 1, 0, '2016-08-07 01:48:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '57a67863e767b', 'Liton dash', 'liton', 'liton@hotmail.com', 'liton098', 0, 1, 0, '2016-08-07 01:53:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
