<?php
/*explode=> breaks a string into an array,
 * Note: Seperator parameter cannot be an empty string
 * this function is binary safe
 */
echo 'Explode: ';
$str = "I am a pupil of BITM. I work at php.I want to compleate PHP Laravel Framework course.";

$explodedString = explode(" ",$str) ;   // first parameter is delimitar, 2nd string, 3rd limit
//echo imm();
echo '<pre>';
print_r($explodedString);


/* impoode=> return a string from the elements of an array
 * separator parameter of this function is optional but an array is recommended
 * implode(separator, array);
 *  */
echo 'Implode: ';

$implodedString = implode(" ", $explodedString); // 1st parameter glue, 2nd array of pieces

echo $implodedString;

echo '</pre>';

// str_pad function

$string = "Hello Bangladesh Pupil";
$stringPadString = str_pad($string, 45, " bitmphpcourse"); // first parameter input, 2nd padLeangth 3rd padString

echo 'String Pad : '.$stringPadString;

echo '<br/>';

// addslashes function=> add a backslashin front of eachdouble wuote (")or predefined characters.

echo 'Addslashes: ';
$str = "Who's Professor Moriarty ?";
echo $str . "This is not safe in a database ";
echo addslashes($str). " This is safe in a database.";

echo '<br/>';

/* addcslashes function=> returns a string with backslashs in front of the specified characters, it has two parameter.
addcslashes(string, characters) */
echo 'Addcslash: ';
$simpleString = 'wellcome to BITM PHP course';
echo $simpleString . '<br/>';
echo addcslashes($simpleString, 'c').'<br/>';
echo addcslashes($simpleString, 'P').'<br/>';

/*
 * htmlentities-> convert some characters to HTML entities
 * the HTML output of the code above will be (View Source)
 */
echo 'HTMLEntities: ';
$string = "<© W3Sçh°°¦§>";
echo htmlentities($string).'<br/>';

