<?php
    include_once 'debug.php';
    
$array1 = array("a"=>array("red"),"b"=>array("green","blue"),);
$array2 = array("a"=>array("yellow"),"b"=>array("black"));

$data = array_replace_recursive($array1, $array2);  // first parameter(array) is required others optional


echo 'array_replace_recursive: (Replace the values of the first array with the values from the second array recursively)';
debug($data);


$a1=array("a"=>array("red","green"),"b"=>array("green","blue","yellow"));
$a2=array("a"=>array("yellow"),"b"=>array("black"));
$a3=array("a"=>array("orange"),"b"=>array("burgundy"));

debug(array_replace_recursive($a1,$a2,$a3));