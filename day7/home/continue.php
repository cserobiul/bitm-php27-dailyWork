<?php

$arr = array(1, 2, 3,4,5,6,7,8,9);
foreach($arr as $number) {
  if($number == 2) {
    continue;
  }
  print $number;
  echo '<br>';
}


echo '<br>';

$stack = array('first', 'second', 'third', 'fourth', 'fifth');

foreach($stack AS $value){
    if($value == 'second')continue;
    if($value == 'fifth')break;
    echo $value.'<br>';
} 