<?php
$subject = array("computer","bba","eee","txt","mba");
echo "Normal Count: ".sizeof($subject);
echo "<br/> Another example: <br/>";
$sub = array("computer"=>array("desktop","laptop"), "txt"=>array("raw","wei"), "eee"=>array("circuit","dld"));
echo "Normal count: ".sizeof($sub);
echo "<br/>";
echo "Recursive count: ".sizeof($sub,1);